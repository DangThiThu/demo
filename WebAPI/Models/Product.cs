﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json.Linq;
using WebAPI.Models.ViewModel;

namespace WebAPI.Models
{
    public class Product
    {
        [Key]
        public int id { get; set; }
        [Required]
        [Display(Name = "Tên Sản Phẩm")]
        public string name { get; set; }
        public string alias { get; set; }

        [Required]
        public string image { get; set; }

        [Display(Name = "Mô Tả")]
        public string describe { get; set; }
        public bool delete { get; set; }

        [ForeignKey("brandID")]
        public int brandID { get; set; }
        [Display(Name = "Nhãn hiệu")]
        public virtual Brands brands { get; set; }

        [ForeignKey("typeID")]
        public int typeID { get; set; }

        [Display(Name = "Loại")]
        public virtual Types types { get; set; }

        [ForeignKey("materialID")]
        public int materialID { get; set; }
        [Display(Name = "Vật liệu")]
        public virtual Materials materials { get; set; }

        public int price { get; set; }


        public int percentOff { get; set; }

      
        public int collectionID { get; set; }
    
        public JObject tranform()
        {
            JObject obj = new JObject();
            int percent = this.percentOff;
            int price = this.price;
            int money = percent * price / 100;

            obj.Add("finalMoney", this.price- money);
            obj.Add("id", this.id);
            obj.Add("name", this.name);
            obj.Add("image", this.image);
            obj.Add("describe", this.describe);
            obj.Add("brandId", this.brandID);
            obj.Add("typeId", this.typeID);
            obj.Add("materialId", this.materialID);
            obj.Add("price", this.price);
            obj.Add("percentOff", this.percentOff);
            obj.Add("delete", this.delete);
            obj.Add("collection", this.collectionID);

            return obj;
        }

        public JObject tranformProduct(int soluong,Types type, Materials material, Brands brand)
        {
            JObject obj = new JObject();
            int percent = this.percentOff;
            int price = this.price;
            int money = percent * price / 100;

            obj.Add("finalMoney", this.price - money);
            obj.Add("id", this.id);
            obj.Add("name", this.name);
            obj.Add("image", this.image);
            obj.Add("describe", this.describe);
            obj.Add("brand", JToken.FromObject(brand));
            obj.Add("type", JToken.FromObject(type));
            obj.Add("material", JToken.FromObject(material));
            obj.Add("price", this.price);
            obj.Add("percentOff", this.percentOff);
            obj.Add("delete", this.delete);
            obj.Add("collection", this.collectionID);
            obj.Add("amount", soluong);

            return obj;
        }
        public ViewProductDetail tranfromCollection(List<Product> listCollection, Product product,string brandName, string typeName, string materialName)
        {

            ViewProductDetail obj = new ViewProductDetail();
            obj.product = product;
            obj.brandName = brandName;
            obj.typeName = typeName;
            obj.materialName = materialName;
            obj.collection = listCollection;
            return obj;
        }

        public JObject tranfromObject(List<JObject> listObject, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listObject));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }






    }
}
