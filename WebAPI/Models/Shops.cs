﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Shops
    {
        [Key]
        public int id { get; set; }
        [Required]

        [Display(Name = "Tên cửa hàng")]
        public string name { get; set; }
        public string alias { get; set; }
        [Required]

        [Display(Name = "Địa chỉ")]
        public string address { get; set; }
        public bool delete { get; set; }

        public JObject tranform(List<Shops> listShop, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listShop));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }
    }
}
