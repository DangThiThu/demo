﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;

namespace WebAPI.Models
{
    public class Materials
    {
        [Key]
        public int id { get; set; }
        [Required]
        [Display(Name = "Tên Vật Liệu")]
        public string name { get; set; }
        public string alias { get; set; }
     //   public virtual ICollection<Product> product { get; set; }
        public string discription { get; set; }
        public bool delete { get; set; }

        public JObject tranform(List<Materials> listMaterials, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listMaterials));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }
    }
}
