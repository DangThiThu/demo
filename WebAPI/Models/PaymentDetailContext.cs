﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models.Authentication;

namespace WebAPI.Models
{
    public class PaymentDetailContext :IdentityDbContext
    {
        public PaymentDetailContext(DbContextOptions<PaymentDetailContext> options):base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {     

            base.OnModelCreating(builder);
        }
        public DbSet<Order> Orders{ set; get; }
        public DbSet<Brands> Brands { get; set; }
        public DbSet<Materials> Materials { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Types> Types { set; get; }

        public DbSet<Order> Order { get; set; }

        public DbSet<ApplicationUser> ApplicationsUser { set; get; }
        public DbSet<OrderDetail> OrderDetail { get; set; }

        public DbSet<Promotions> Promotions { get; set; }

        public DbSet<Customer> Customer { get; set; }
        public DbSet<Shops> Shops { get; set; }

        public DbSet<NumberOfProduct> NumberOfProduct { get; set; }
        public DbSet<Collections> Collections { get; set; }
        public DbSet<ShoppingCart> ShoppingCart { get; set; }
        public DbSet<Request> Request { get; set; }










    }
}
