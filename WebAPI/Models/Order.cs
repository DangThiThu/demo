﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models.Authentication;

namespace WebAPI.Models
{
    public class Order
    {
        public int id { get; set; }

        [Display(Name = "Sales Person")]
        public string salePersonId { get; set; }

        public string nameSalePerson { get; set; }

        [Display(Name = "Appointment Date")]

        public DateTime appointment { get; set; }

        [Display(Name = "Customer Name")]

        public string customerName { get; set; }
        [Display(Name = "Customer Phone Number")]
        public string customerPhoneNumber { get; set; }
        [Display(Name = "Customer Email")]
        public string customerEmail { get; set; }
        [Display(Name = "Confirmed")]
   
        public string address { get; set; }
      

        public virtual ICollection<OrderDetail> orderDetails { get; set; }
        public int promotionID { set; get; }
        //[ForeignKey("promotionID")]
        //public virtual Promotions promotions { set; get; }

        public int totalmoney { get; set; }

        public int realmoney { get; set; }

        public int moneyIsReduced { get; set; }

        [ForeignKey("shopID")]
        public int shopID { get; set; }
        public virtual Shops shops { get; set; }
        public bool delete { get; set; }
        public bool isConfirmed { get; set; }
        public bool delivering { get; set; }
        public bool success_deliver { get; set; }
        public bool isPayment { get; set; }
        public bool cancel { get; set; }
        public bool isCompleted { get; set; }



        public JObject tranform(string name,List<OrderDetail> listOrderDetail, Promotions Promotion)
       {

            JObject obj = new JObject();
            string promotionCode = "";
            if (Promotion !=null) {
                promotionCode = Promotion.promotionCode;
            }
            obj.Add("id", this.id);
            obj.Add("salePersonId",this.salePersonId);
            obj.Add("salePerson", name);
            obj.Add("appointment", this.appointment);           
            obj.Add("customerName", this.customerName);
            obj.Add("customerPhoneNumber", this.customerPhoneNumber);
            obj.Add("customerEmail", this.customerEmail);
            obj.Add("isConfirmed", this.isConfirmed);
            obj.Add("isCompleted", this.isCompleted);
            obj.Add("address", this.address);
            obj.Add("promotionCode", promotionCode);
            obj.Add("totalmoney", this.totalmoney);
            obj.Add("realmoney", this.realmoney);
            obj.Add("moneyIsReduced", this.moneyIsReduced);
            obj.Add("listOrderDetails", JToken.FromObject(listOrderDetail));
            obj.Add("shopId", this.shopID);
            obj.Add("isPayment", this.isPayment);
            obj.Add("delivering", this.delivering);
            obj.Add("success_deliver", this.success_deliver);
            obj.Add("cancel", this.cancel);
            // obj.Add("listOrderDetails", JToken.FromObject(this.orderDetails));
            obj.Add("delete", this.delete);

           return obj;
      }
        public JObject tranfromObject(List<JObject> listObject, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listObject));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }

        public static implicit operator Order(ActionResult<Order> v)
        {
            throw new NotImplementedException();
        }
    }
}
