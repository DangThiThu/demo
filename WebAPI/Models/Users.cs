﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Users
    {
        public  string name { get; set; }
        public string email { get; set; }
        public string provider { get; set; }
        public string provideId { get; set; }
        public string token { get; set; }
        public string idToken { get; set; }
    }
}
