﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class LoginSocial
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string provider { get; set; }
        public string image { get; set; }
        public string token { get; set; }
        public string shopID { get; set; }
     
        
    }
}
