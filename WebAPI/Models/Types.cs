﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Linq;
using System;

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Types
    {
        [Key]
        public int id { get; set; }
        [Required]

        [Display(Name = "Tên Loại")]
        public string name { get; set; }
        public string alias { get; set; }

        //public virtual ICollection<Product> product { get; set; }
        public string discription { get; set; }
        public bool delete { get; set; }

        public JObject tranform(List<Types> listTypes,Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listTypes));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }
    }
   
}
