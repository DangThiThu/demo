﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class OrderDetail
    {
        public int id { get; set; } 
        public string name { get; set; }
        [ForeignKey("Order")]
        public int? orderId { get; set; }
      //  public virtual Order order { get; set; }
        [ForeignKey("Product")]
        public int? productID { get; set; }
        public virtual Product product { get; set; }
        public string image { get; set; }
        public decimal price { set; get; }

        public int quantityOfProduct { get; set; }
        public bool delete { get; set; }
      


    }
}
