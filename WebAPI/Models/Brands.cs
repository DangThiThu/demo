﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;

namespace WebAPI.Models
{
    public class Brands
    {
        [Key]
        public int id { get; set; }
        [Required]

        [Display(Name = "Tên Hãng")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Quốc Gia")]
        public string country { get; set; }
        public string alias { get; set; }
        public string image { get; set; }

        public virtual ICollection<Product> product { get; set; }
        public string discription { get; set; }
        public bool delete { get; set; }

        public JObject tranform(List<Brands> listBrands, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listBrands));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }
    }
}
