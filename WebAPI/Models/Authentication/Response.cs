﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Authentication
{
    public class Response
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}
