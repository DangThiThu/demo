﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Authentication
{
    public class ApplicationUserModel
    {
        [Required(ErrorMessage = "UserName is required")]
        public string userName { set; get; }

        [Required(ErrorMessage = "Email is required")]
        public string email { set; get; }

        [Required(ErrorMessage = "Password is required")]
        public string password { set; get; }
        public int shopID { get; set; }

        [Required(ErrorMessage = "FullName is required")]
        public string fullName { set; get; }
        public bool delete { get; set; }
       
    }
}
