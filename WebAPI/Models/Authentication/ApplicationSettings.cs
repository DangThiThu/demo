﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Authentication
{
    public class ApplicationSettings
    {
        public string JWT_Secret { set; get; }
        public string Client_Url { set; get; }
    }
}
