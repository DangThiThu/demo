﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Authentication
{
    public class UserRoles
    {
        public const string superAdmin = "SuperAdmin";
        public const string staff = "Staff";
        public const string manager = "Manager";
        public const string customer = "Customer";
    }
}
