﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.Authentication
{
    public class UpdateUser
    {
      
        public string userName { set; get; }     
        public string email { set; get; }
        public string fullName { set; get; }
        public string role { get; set; }
       
    }
}
