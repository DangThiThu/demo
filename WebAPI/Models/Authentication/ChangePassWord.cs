﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class ChangePassWord
    {
        public string id { get; set; }

        public string currentPassword { get; set; }

        public string newPassword { get; set; }

        public string confirmNewPassword { get; set; }
    }
}
