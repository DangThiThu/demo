﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAPI.Models.Authentication
{
    public class ApplicationUser : IdentityUser
    {
        [Column(TypeName = "nvarchar(50)")]
        public string FullName { set; get; }

      
        public bool Delete { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Role { get; set; }
        [NotMapped]
        public bool IsSuperAdmin { get; set; }

        public int shopID { get; set; }

        public JObject tranform()
        {
            JObject obj = new JObject();                          
            obj.Add("username", this.UserName);
            obj.Add("userID", this.Id);
            obj.Add("fullName", this.FullName);
            obj.Add("email", this.Email);
            obj.Add("role", this.Role);
            obj.Add("shopID", this.shopID);
            return obj;
        }

        public JObject tranfromObject(List<JObject> listUser, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listUser));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }
    }

}
