﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Promotions
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Loại khuyến mãi")]
        public string promotionCode { get; set; }
        public string image { get; set; }


        public string discription { get; set; }
        public string alias { get; set; }
        public int condition { get; set; }

        public int moneyOff { get; set; }

        public string startday { get; set; }
        public string endDay { get; set; }
        public virtual ICollection<Order> order { get; set; }
        public bool delete { get; set; }
        public JObject tranform(List<Promotions> listPromotions, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listPromotions));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }



    }
}
