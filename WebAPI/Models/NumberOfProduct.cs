﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json.Linq;

namespace WebAPI.Models
{
    public class NumberOfProduct
    {
        [Key]
        public int id { get; set; }
        [ForeignKey("productID")]
        public int productID { get; set; }
        [ForeignKey("shopID")]
        public int shopID { get; set; }
        public int amount { get; set; }

        public JObject tranform(List<NumberOfProduct> listAvaiable, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listAvaiable));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }

    }
}
