﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class NewProduct
    {
        public int productID;
        public int quantityOfProduct;
        public NewProduct(int productID, int quantityOfProduct)
        {
            this.productId = productID;
            this.amount = quantityOfProduct;
        }

        public int productId { get; set; }
        public int amount { get; set; }
    }
}
