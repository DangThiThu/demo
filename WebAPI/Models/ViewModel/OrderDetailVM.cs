﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class OrderDetailVM
    {
        public int id { get; set; }
        public string name { get; set; }
        public int qty { get; set; }
        public string imUrl { get; set; }
        public int price { get; set; }


    }
}
