﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class FileUploadBrands
    {
        public IFormFile files { set; get; }
        public string name { get; set; }
        public string discription { get; set; }
        public string country { get; set; }
    }
}
