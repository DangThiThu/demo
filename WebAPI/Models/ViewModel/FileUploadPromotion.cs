﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class FileUploadPromotion
    {
        public IFormFile files { set; get; }
        public string promotionCode { get; set; }
        public string discription { get; set; }
        public string condition { get; set; }

        public string moneyOff { get; set; }

        public DateTime startday { get; set; }
        public DateTime endDay { get; set; }
     
       
    }
}
