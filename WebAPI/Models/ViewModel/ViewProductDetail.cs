﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class ViewProductDetail
    {
        public Product product { get; set; }
        public string brandName { get; set; }
        public string typeName { get; set; }
        public string materialName { get; set; }
        public List<Product> collection { get; set; }
    }
}
