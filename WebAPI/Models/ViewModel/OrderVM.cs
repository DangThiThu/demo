﻿using Stripe.Checkout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class OrderVM
    {
        private Session session;

        public OrderVM(Session session)
        {
            this.session = session;
        }

        public OrderDetailVM[] listProducts { get; set; }
        public string name { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string appointment { get; set; }

        public Customer customerInfo { get; set; }
        public string promotionCode { get; set; }
        public int shopID { get; set; }

    }
}
