﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models.ViewModel
{
    public class FileUploadAPI
    {
        public IFormFile files { set; get; }
        public string name { set; get; }
        public string brandId { set; get; }

        public string typeId { set; get; }
        public string materialId { set; get; }
        public string describe { get; set; }

        public string percentOff { get; set; }
        public string price { get; set; }
        public string collectionID { get; set; }
        public int amount { get; set; }
   



    }
}
