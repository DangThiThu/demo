﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;

namespace WebAPI.Models
{
    public class Request
    {
        [Key]
        public int id { get; set; }
      

      
      
        public int  shopId { get; set; }
        public string name { get; set; }
        public string alias { get; set; }
        public int productId { get; set; }
        public int amount { get; set; }
        public DateTime date { get; set; }

        public bool request { get; set; }
        public bool isconfirm { get; set; }
        public bool sending { get; set; }
        public bool receive { get; set; }

        public JObject tranform(List<JObject> listObject, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listObject));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }
    }
}
