﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class ShoppingCart
    {
        public int id { get; set; }
        
        public int customerID { get; set; }
        public int productID { get; set; }

        public int soluong { get; set; }

        public JObject tranform(string image, string name, int price, int percentOff)
        {
          
           
            JObject obj = new JObject();
            obj.Add("productId", this.productID);
            obj.Add("quantity", this.soluong);
            obj.Add("name", name);
            obj.Add("price", price- percentOff);
            obj.Add("image", image);


            return obj;
        }

    }
}
