﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Customer
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string provider { get; set; }
        public string fullname { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public JObject tranform(List<Customer> listUser, Object page)
        {

            JObject obj = new JObject();
            obj.Add("data", JToken.FromObject(listUser));
            obj.Add("total_page", JToken.FromObject(page));
            return obj;
        }


    }
}
