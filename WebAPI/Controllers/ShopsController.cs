﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public ShopsController(PaymentDetailContext context)
        {
            _context = context;
        }

        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }

        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        // GET: api/Shops
        [HttpGet]
        public async Task<ActionResult<List<Object>>> GetShopsByPage(int page, string name)
        {
            List<Shops> shopList = new List<Shops>();
             shopList = await _context.Shops.Where(a => a.delete != true).ToListAsync();
            if(name!=null)
            {
                string keyword = convertToUnSign3(name);
                var listShop = from shop in _context.Shops
                               where shop.alias.Contains(keyword)
                               select shop;

                shopList = new List<Shops>(listShop);
            }
            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, shopList.Count());



            List<Object> newListShop= new List<Object>();
            foreach (var b in shopList)
            {
                List<Shops> type = new List<Shops>((from types in shopList select types).Skip(startIndex).Take(15));
                newListShop.Add(b.tranform(type, pagination));

                break;

            }
            return newListShop;

        }

        //  lấy danh sách shop còn đủ product
        // GET: api/Shops/all_product
        [HttpGet]
        [Route("all_product")]
        public async Task<ActionResult<List<Shops>>> GetShopsEnoughProduct()
        {
            List<Shops> listShop = new List<Shops>(_context.Shops.Where(shop => shop.delete != true));
            List<NumberOfProduct> numberOfProducts = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.amount == 0));
            
            foreach(var item in numberOfProducts)
            {
                listShop = new List<Shops>(_context.Shops.Where(shop => shop.id != item.shopID));

            }
            return listShop;

        }

       
        //lấy danh sách shop còn đủ mặt hàng và số lượng
        [HttpPost]
        // GET: api/Shops/by_product
        [Route("by_product")]
        public async Task<ActionResult<List<Shops>>> GetShopsByProudct(List<NewProduct>listProduct)
        {
            List<NumberOfProduct> numberOfProduct = new List<NumberOfProduct>(_context.NumberOfProduct);
            foreach(var item in listProduct)
            {
                numberOfProduct = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.productID == item.productId && a.amount >= item.amount));
            }
            List<Shops> newList = new List<Shops>();
            foreach(var b in numberOfProduct)
            {
                Shops shop = _context.Shops.Where(a => a.delete != true && a.id == b.shopID).FirstOrDefault();

                newList.Add(shop);
            }
            return newList;


        }

        // GET: api/Shops/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Shops>> GetShops(int id)
        {
            var shops = await _context.Shops.FindAsync(id);

            if (shops == null)
            {
                return NotFound();
            }

            return shops;
        }

        // PUT: api/Shops/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShops(int id, Shops shops)
        {
            if (id != shops.id)
            {
                return BadRequest();
            }
            shops.alias = convertToUnSign3(shops.name);

            _context.Entry(shops).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShopsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Shops

        [HttpPost]
        public async Task<ActionResult<Shops>> PostShops(Shops shops)
        {
            shops.alias = convertToUnSign3(shops.name);
            _context.Shops.Add(shops);
            await _context.SaveChangesAsync();

            //thêm sản phẩm vào shop mới tạo
            List<Product> listProducts = new List<Product>(_context.Product.Where(a => a.delete != true));
            foreach (var pro in listProducts)
            {
                NumberOfProduct numberOfProduct = new NumberOfProduct();
                numberOfProduct.shopID = shops.id;
                numberOfProduct.productID = pro.id;
                numberOfProduct.amount = 0;
                _context.NumberOfProduct.Add(numberOfProduct);
                await _context.SaveChangesAsync();

            }
           
            return CreatedAtAction("GetShops", new { id = shops.id }, shops);
        }

        // DELETE: api/Shops/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Shops>> DeleteShops(int id)
        {
            var shops = await _context.Shops.FindAsync(id);
            if (shops == null)
            {
                return NotFound();
            }
            var result = await _context.Order.Where(a => a.delete != true).SingleOrDefaultAsync(x => x.shopID == id);
            if (result == null)
            {
                shops.delete = true;
                _context.Update(shops);
                await _context.SaveChangesAsync();
            }
            else
            {
                return null;
            }       
            return shops;
          
        }

        private bool ShopsExists(int id)
        {
            return _context.Shops.Any(e => e.id == id);
        }
    }
}
