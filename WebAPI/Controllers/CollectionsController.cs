﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.Authentication;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public CollectionsController(PaymentDetailContext context)
        {
            _context = context;
        }

        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        // GET: api/Collections
        [HttpGet]
        public async Task<ActionResult<List<Object>>> GetAllCollections(int page, string name)
        {
            List<Collections> listCollections = new List<Collections>();
            listCollections= await _context.Collections.Where(a=>a.delete!=true).ToListAsync();
            if(name!=null)
            {
                string keyword = convertToUnSign3(name);
                var newlistCollection = from collections in _context.Collections
                                     where collections.alias.Contains(keyword)
                                     select collections;

                listCollections = new List<Collections>(newlistCollection);
            }
            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, listCollections.Count());



            List<Object> newListCollection = new List<Object>();
            foreach (var b in listCollections)
            {
                List<Collections> collection = new List<Collections>((from types in listCollections select types).Skip(startIndex).Take(15));
                newListCollection.Add(b.tranform(collection, pagination));

                break;

            }
            return newListCollection;
        }

        // GET: api/Collections/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Collections>> GetCollections(int id)
        {
            var collections = await _context.Collections.FindAsync(id);

            if (collections == null)
            {
                return NotFound();
            }

            return collections;
        }
        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }

        // PUT: api/Collections/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCollections(int id, Collections collections)
        {
            if (id != collections.id)
            {
                return BadRequest();
            }
            collections.alias = convertToUnSign3(collections.name);
            _context.Entry(collections).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CollectionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Collections
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Collections>> PostCollections(Collections collections)
        {
            collections.alias = convertToUnSign3(collections.name);
            _context.Collections.Add(collections);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCollections", new { id = collections.id }, collections);
        }

        // DELETE: api/Collections/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Collections>> DeleteCollections(int id)
        {
            var collections = await _context.Collections.FindAsync(id);
            if (collections == null)
            {
                return NotFound();
            }

            _context.Collections.Remove(collections);
            await _context.SaveChangesAsync();

            return collections;
        }

        private bool CollectionsExists(int id)
        {
            return _context.Collections.Any(e => e.id == id);
        }
    }
}
