﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NumberOfProductController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public NumberOfProductController(PaymentDetailContext context)
        {
            _context = context;
        }

        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        // GET: api/Avaiables
        [HttpGet]
        public async Task<ActionResult<List<Object>>> GetAvaiablesByPage(int page)
        {
            var listAvaiable= await _context.NumberOfProduct.ToListAsync();
            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, listAvaiable.Count());



            List<Object> newListAvaiable = new List<Object>();
            foreach (var b in listAvaiable)
            {
                List<NumberOfProduct> avaiables = new List<NumberOfProduct>((from types in listAvaiable select types).Skip(startIndex).Take(15));
                newListAvaiable.Add(b.tranform(avaiables, pagination));

                break;

            }
            return newListAvaiable;
        }

        // GET: api/Avaiables/5
        [HttpGet("{id}")]
        public async Task<ActionResult<NumberOfProduct>> GetAvaiable(int id)
        {
            var avaiable = await _context.NumberOfProduct.FindAsync(id);

            if (avaiable == null)
            {
                return NotFound();
            }

            return avaiable;
        }


        //Cập nhật số lượng product sau khi thêm new product
        // GET: api/Avaiables/update-amount
        [HttpPost]
        [Route("update-amount")]
        public async Task<ActionResult<List<NumberOfProduct>>> UpdateNumberOfProduct(int productId,int shopID, int soluong)
        {
            List<NumberOfProduct> listNumberOfProduct = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.productID == productId && a.shopID==shopID));
            foreach(var item in listNumberOfProduct)
            {
                item.amount = soluong;
                _context.Update(item);

                await _context.SaveChangesAsync();
            }

            return listNumberOfProduct;
        }

        // PUT: api/Avaiables/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAvaiable(int id, NumberOfProduct avaiable)
        {
            if (id != avaiable.id)
            {
                return BadRequest();
            }

            _context.Entry(avaiable).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AvaiableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Avaiables
        [HttpPost]
        public async Task<ActionResult<NumberOfProduct>> PostAvaiable(NumberOfProduct avaiable)
        {
            _context.NumberOfProduct.Add(avaiable);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAvaiable", new { id = avaiable.id }, avaiable);
        }

        // DELETE: api/Avaiables/delete
      [HttpDelete]
      [Route("delete")]
    
      [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<NumberOfProduct>> DeleteAvaiable(int id)
        {
            var avaiable = await _context.NumberOfProduct.FindAsync(id);
            if (avaiable == null)
            {
                return NotFound();
            }
          
            _context.NumberOfProduct.Remove(avaiable);
            return avaiable;
        }

        private bool AvaiableExists(int id)
        {
            return _context.NumberOfProduct.Any(e => e.id == id);
        }

        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }

        //Lấy danh sách sản phẩm sắp hết hàng
        [HttpGet]
        [Route("out-of-stock")]
       // [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<List <JObject>>> GetOutofStock(int shopID, int page, string name)
        {
            int startIndex = (page - 1) * 15;
            int total = 0;
            List<Product> listProduct = new List<Product>();
            var listRequest = _context.Request.Where(p=>p.shopId == shopID && p.receive == false).Select(x => x.productId).ToArray();
            List<NumberOfProduct> listAvaiable = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.shopID == shopID && a.amount <=10 && !listRequest.Contains(a.productID)));


            foreach (var item in listAvaiable)
            {
                Product product = _context.Product.Where(a => a.id == item.productID && a.delete != true).FirstOrDefault();
                if (product != null)
                {
                    listProduct.Add(product);
                }
            }

            if (name != null)
            {
                string keyword = convertToUnSign3(name);
                var newList = listProduct.Where(a => a != null && a.alias.Contains(keyword)).ToList();
                listProduct = new List<Product>(newList);
            }
            total = listProduct.Count();
            listProduct = new List<Product>((listProduct).Skip(startIndex).Take(15));


            List<JObject> listJObject = new List<JObject>();
            foreach (var b in listProduct)
            {
                var numberOfProduct = await _context.NumberOfProduct.Where(a => a.shopID == shopID && a.productID == b.id).FirstOrDefaultAsync();
                Types type = _context.Types.Where(a => a.delete != true && a.id == b.typeID).FirstOrDefault();
                Materials material = _context.Materials.Where(ma => ma.delete != true && ma.id == b.materialID).FirstOrDefault();
                Brands brand = _context.Brands.Where(br => br.delete != true && br.id == b.brandID).FirstOrDefault();


                listJObject.Add(b.tranformProduct(numberOfProduct.amount, type, material, brand));
            }



            Object pagination = GetPagiantion(page, total);

            List<JObject> newListObject = new List<JObject>();
            foreach (var b in listProduct)
            {

                newListObject.Add(b.tranfromObject(listJObject, pagination));

                break;

            }

            return newListObject;
        }

    }
}
