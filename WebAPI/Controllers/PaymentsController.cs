﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Stripe;
using Stripe.Checkout;
using WebAPI.Models;
using WebAPI.Models.Authentication;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly StripeSettings _stripeSettings;
        private readonly PaymentDetailContext _context;
        public static IWebHostEnvironment _IWebHostEnvironment;

        const string secret = "sk_test_51IyuQoKSbsx5h2tXxIIvx9KVbXto4vDNukwKqBgEPwaK9UIIanVUxeBy2Rn90TIP95YfpyuUJfBqeXjqJKqyjtDZ00FO7RDLRZ";
        public PaymentsController(IOptions<StripeSettings> stripeSettings,IWebHostEnvironment iWebHostEnvironment, PaymentDetailContext context)
        {
            _IWebHostEnvironment = iWebHostEnvironment;
            _context = context;
            _stripeSettings = stripeSettings.Value;
        }
        [HttpPost("create-checkout-session")]
        public async Task<ActionResult> Create(OrderVM orderVM)
        {
           Models.Order newOrder= await PostOrder(orderVM);
            var couponOptions = new CouponCreateOptions
            {
                AmountOff = 200000,
                Currency = "vnd",
            };
            var couponService = new CouponService();
            Coupon coupon = couponService.Create(couponOptions);

            List<SessionLineItemOptions> newLineItems = new List<SessionLineItemOptions>();
            for (int k = 0; k < orderVM.listProducts.Length; k++)
            {

                SessionLineItemOptions item = new SessionLineItemOptions
                {
                    PriceData = new SessionLineItemPriceDataOptions
                    {
                        UnitAmount = orderVM.listProducts[k].price,
                        Currency = "vnd",
                        ProductData = new SessionLineItemPriceDataProductDataOptions
                        {
                            Name = orderVM.listProducts[k].name,
                        },
                    },
                    Quantity = orderVM.listProducts[k].qty,
                    
                };
               
                newLineItems.Add(item);


            }

          
            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string>
                {
                  "card",
                },
                 LineItems = newLineItems,
                Mode = "payment",
                Discounts = new List<SessionDiscountOptions>
                  {
                    new SessionDiscountOptions
                    {
                      Coupon = coupon.Id,
                    },
                  },

                SuccessUrl = "https://deploy-angular-2afbb.web.app/checkout/success/" + newOrder.id,
                CancelUrl = "https://deploy-angular-2afbb.web.app/checkout/fail/" + +newOrder.id,
            };
            var service = new SessionService();
            Session session = service.Create(options);
            return new ObjectResult(session) { StatusCode = 200 };
        }


        private async Task<ActionResult<Models.Order>> PostOrder(OrderVM orderVM)
        {

            try
            {
                Models.Order order = new Models.Order();
                order.customerName = orderVM.name;
                order.customerPhoneNumber = orderVM.phoneNumber;
                order.customerEmail = orderVM.email;
                order.appointment = DateTime.Parse(orderVM.appointment);
                order.address = orderVM.address;

                if (orderVM.shopID != null)
                {

                    order.shopID = orderVM.shopID;
                }

                if (orderVM.customerInfo != null)
                {
                    string customerEmail = orderVM.customerInfo.email;
                    var customer = await _context.Customer.Where(a => a.email == customerEmail).FirstOrDefaultAsync();
                    if (customer != null)
                    {
                        customer.address = orderVM.address;
                        customer.fullname = orderVM.name;
                        customer.phone = orderVM.phoneNumber;
                        _context.Update(customer);
                        await _context.SaveChangesAsync();
                    }


                }
                else
                {
                    Models.Customer CustomerInFo = new Models.Customer();
                    CustomerInFo.name = orderVM.name;
                    CustomerInFo.email = orderVM.email;
                    CustomerInFo.fullname = orderVM.name;
                    CustomerInFo.address = orderVM.address;
                    CustomerInFo.phone = orderVM.phoneNumber;
                    _context.Customer.Add(CustomerInFo);
                    await _context.SaveChangesAsync();
                }
                string promotionCODE = orderVM.promotionCode;
                var promotion = await _context.Promotions.Where(a => a.promotionCode == promotionCODE && a.delete != true).FirstOrDefaultAsync();
                if (promotionCODE != null)
                {

                    order.promotionID = promotion.id != null ? promotion.id : 0;
                }

                int realMoney = 0;

                for (int i = 0; i < orderVM.listProducts.Length; i++)
                {
                    realMoney = realMoney + orderVM.listProducts[i].qty * orderVM.listProducts[i].price;

                }
                order.realmoney = realMoney;
                int moneyIsReduced = 0;

                //  var promotion = await _context.Promotions.Where(a => a.id == promotionID && a.delete != true).FirstOrDefaultAsync();

                if (orderVM.promotionCode != null && promotion != null)
                {

                    DateTime startTime = DateTime.Parse(promotion.startday.ToString());
                    DateTime endTime = DateTime.Parse(promotion.endDay.ToString());
                    DateTime appoiment = order.appointment;
                    if (endTime.Date >= appoiment.Date && startTime.Date <= appoiment.Date)
                    {
                        moneyIsReduced = promotion.moneyOff;
                    }
                }

                order.moneyIsReduced = moneyIsReduced;
                order.totalmoney = realMoney - moneyIsReduced;

                _context.Order.Add(order);
                await _context.SaveChangesAsync();
                int orderID = order.id;

                for (int k = 0; k < orderVM.listProducts.Length; k++)
                {
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.orderId = orderID;
                    orderDetail.productID = orderVM.listProducts[k].id;
                    orderDetail.name = orderVM.listProducts[k].name;
                    orderDetail.image = orderVM.listProducts[k].imUrl;
                    orderDetail.price = orderVM.listProducts[k].price;
                    orderDetail.quantityOfProduct = orderVM.listProducts[k].qty;


                    //giam so luong cua san pham da bán
                    var numberOfProduct = _context.NumberOfProduct.Where(a => a.productID == orderVM.listProducts[k].id && a.shopID == orderVM.shopID).FirstOrDefault();
                    int newAmount = numberOfProduct.amount - orderVM.listProducts[k].qty;
                    numberOfProduct.amount = newAmount;
                    _context.Update(numberOfProduct);
                    await _context.SaveChangesAsync();
                    // checkQuantityOfProduct(orderVM.listProducts[k].id, orderVM.listProducts[k].qty, orderVM.shopID);

                    _context.OrderDetail.Add(orderDetail);
                    await _context.SaveChangesAsync();


                }
                return order;

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


    }
}