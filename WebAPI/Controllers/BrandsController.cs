﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.Authentication;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class BrandsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;
        public static IWebHostEnvironment _IWebHostEnvironment;

        public BrandsController(IWebHostEnvironment iWebHostEnvironment,PaymentDetailContext context)
        {
            _IWebHostEnvironment = iWebHostEnvironment;
            _context = context;
        }

        private Object GetPagiantion(int page, int totalList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }



        // GET: api/Brands
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<List<Object>>> GetBrandslByPage(int page, string name)
        {
            List<Brands> newList = new List<Brands>();
             newList = await _context.Brands.Where(a => a.delete != true).ToListAsync();
            if(name !=null)
            {
                string keyword = convertToUnSign3(name);
                var newBrands = from brands in _context.Brands
                                where brands.alias.Contains(keyword)
                                select brands;
                newList = new List<Brands>(newBrands);
            }

            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, newList.Count());



            List<Object> newListType = new List<Object>();
            foreach (var b in newList)
            {
                List<Brands> newData = new List<Brands>((from types in newList select types).Skip(startIndex).Take(15));
                newListType.Add(b.tranform(newData, pagination));

                break;

            }
            return newListType;
        }


        //GET Brands contain product
        // GET: api/Brands/5
        [HttpGet]
        [Route("enough-product")]

        public async Task<ActionResult<List<Brands>>> GetBrandsHaveProduct()
        {
            List<Brands> listBrands = new List<Brands>(_context.Brands.Where(a => a.delete != true && a.product.Count > 0));


            return listBrands;
        }


        // GET: api/Brands/5
        [HttpGet("{id}")]
      
        public async Task<ActionResult<Brands>> GetBrands(int id)
        {
            var brands = await _context.Brands.FindAsync(id);

            if (brands == null)
            {
                return NotFound();
            }

            return brands;
        }

        // PUT: api/Brands/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Brands>> PutBrands(int id, [FromForm] FileUploadBrands obj)
        {
            string webRootPath = _IWebHostEnvironment.WebRootPath;
            var brand = await _context.Brands.FindAsync(id);
            brand.name = obj.name;
            brand.country = obj.country;
            brand.discription = obj.discription;
            brand.alias = convertToUnSign3(obj.name);
            if (obj.files != null)
            {

                if (!Directory.Exists(_IWebHostEnvironment.WebRootPath + "/Upload/"))
                {
                    Directory.CreateDirectory(_IWebHostEnvironment.WebRootPath + "/Upload/");
                }
                var extension_old = brand.image;
                if (System.IO.File.Exists((webRootPath + extension_old)))
                {
                    System.IO.File.Delete((webRootPath + extension_old));
                }

                using (FileStream fileStream = System.IO.File.Create(webRootPath + "/Upload/" + obj.files.FileName))
                {
                    obj.files.CopyTo(fileStream);
                    fileStream.Flush();


                    if (obj.files.FileName != null)
                    {
                        brand.image = "/Upload/" + obj.files.FileName;

                    }
                }
            }
            _context.Update(brand);

            await _context.SaveChangesAsync();
            return brand;

        }

        // POST: api/Brands
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = UserRoles.superAdmin)]
        [HttpPost]
     //   [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Brands>> PostBrands([FromForm] FileUploadBrands obj)
        {
            Brands brands = new Brands();
            brands.name = obj.name;
            brands.country = obj.country;
            brands.discription = obj.discription;
            brands.alias = convertToUnSign3(obj.name);
            if (obj.files != null)
            {
                if (!Directory.Exists(_IWebHostEnvironment.WebRootPath + "/Upload/"))
                {
                    Directory.CreateDirectory(_IWebHostEnvironment.WebRootPath + "/Upload/");
                }
                using (FileStream fileStream = System.IO.File.Create(_IWebHostEnvironment.WebRootPath + "/Upload/" + obj.files.FileName))
                {
                    obj.files.CopyTo(fileStream);
                    fileStream.Flush();

                    brands.image = "/Upload/" + obj.files.FileName;
                    // newProduct.image = Hepler.getImage(obj.files);


                }
            }
            _context.Brands.Add(brands);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBrands", new { id = brands.id }, brands);
        }

        // DELETE: api/Brands/5
        [HttpDelete("{id}")]
         [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Brands>> DeleteBrands(int id)
        {
            var brands = await _context.Brands.FindAsync(id);
            if (brands == null)
            {
                return NotFound();
            }
            var product = await _context.Product.Where(a => a.delete != true).SingleOrDefaultAsync(x => x.brandID == id);
            if (product == null)
            {
                brands.delete = true;
                _context.Update(brands);
                await _context.SaveChangesAsync();

               
            }
            else
            {
                return null;
            }
            return brands;




            // _context.Brands.Remove(brands);

        }

        private bool BrandsExists(int id)
        {
            return _context.Brands.Any(e => e.id == id);
        }
    }
}
