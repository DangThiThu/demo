﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;
using WebAPI.Models.ViewModel;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Runtime.InteropServices.WindowsRuntime;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Text;
using static WebAPI.Constants.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromotionsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;
        public static IWebHostEnvironment _IWebHostEnvironment;

        public PromotionsController(IWebHostEnvironment iWebHostEnvironment,PaymentDetailContext context)
        {
            _IWebHostEnvironment = iWebHostEnvironment;
            _context = context;
        }

        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        // GET: api/Promotions
        [HttpGet]
     //   [Authorize(Roles = "SuperAdmin,Staff,Manager")]
        public async Task<ActionResult<List<Object>>> GetPromotions(int page, string code)
        {
          var listPromotions= await _context.Promotions.Where(a=>a.delete!=true).ToListAsync();

            if (code != null)
            {
                string keyword = convertToUnSign3(code);
                var newPromotions = from promotion in _context.Promotions
                                  where promotion.alias.Contains(keyword)
                                  select promotion;

                listPromotions = new List<Promotions>(newPromotions);

            }

            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, listPromotions.Count());



            List<Object> newListPromotions = new List<Object>();
            foreach (var b in listPromotions)
            {
                List<Promotions> promotion = new List<Promotions>((from promo in listPromotions select promo).Skip(startIndex).Take(15));
                newListPromotions.Add(b.tranform(promotion, pagination));

                break;

            }
            return newListPromotions;
        }

        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }


        // GET: api/Promotions/
        [HttpGet]
        [Route("available")]
        //  [Authorize(Roles = "SuperAdmin,Staff,Manager")]
        public async Task<ActionResult<List<Promotions>>> GetPromotionsAvailable()
        {
            DateTime now = DateTime.Now;
            var result= await _context.Promotions.Where(a => a.delete != true).ToListAsync();
           
            List<Promotions> promotions = new List<Promotions>();
            foreach (var b in result)
            {

                DateTime startTime = DateTime.Parse(b.startday);
                DateTime endTime = DateTime.Parse(b.endDay);
                if (endTime.Date > now.Date && startTime< now.Date)
                {
                    promotions.Add(b);
                }
            }
            return promotions;
        }



       
        // GET: api/Promotions/5
        [HttpGet("{id}")]
        [Authorize(Roles = "SuperAdmin,Staff,Manager")]
        public async Task<ActionResult<Promotions>> GetDetailPromotion(int id)
        {
            
            var promotions = await _context.Promotions.FindAsync(id);

            if (promotions == null)
            {
                return NotFound();
            }

            return promotions;
        }

        //Search promotion theo ma code
        // GET: api/Promotions/code/
        [HttpGet]
        [Route("code")]
       
        public async Task<ActionResult<Promotions>> GetDetailPromotionByCode(string code)
        {

            var promotions = await _context.Promotions.Where(a=>a.delete!=true && a.promotionCode == code).FirstOrDefaultAsync();

            if (promotions == null)
            {
                return NotFound();
            }

            return promotions;
        }




        // PUT: api/Promotions/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<IActionResult> PutPromotions(int id, [FromForm] FileUploadPromotion obj)
        {
          
            string webRootPath = _IWebHostEnvironment.WebRootPath;

            try
            {
                var newPromotion = await _context.Promotions.FindAsync(id);
                newPromotion.promotionCode = obj.promotionCode;
                newPromotion.discription = obj.discription;
                newPromotion.condition = obj.condition != null ? Int32.Parse(obj.condition) : 0;
                newPromotion.moneyOff = obj.moneyOff != null ? Int32.Parse(obj.moneyOff) : 0;
                DateTime startTime = DateTime.Parse(obj.startday.ToString("yyyy-MM-dd"));
                DateTime endTime = DateTime.Parse(obj.endDay.ToString("yyyy-MM-dd"));
                //   String.Format("{0:y yy yyy yyyy}", obj.startday);
                newPromotion.startday = startTime.ToString("yyyy-MM-dd");
                newPromotion.endDay = endTime.ToString("yyyy-MM-dd");

                if (obj.files != null)
                {

                    if (!Directory.Exists(_IWebHostEnvironment.WebRootPath + "/Upload/"))
                    {
                        Directory.CreateDirectory(_IWebHostEnvironment.WebRootPath + "/Upload/");
                    }
                    var extension_old = newPromotion.image;
                    if (System.IO.File.Exists((webRootPath + extension_old)))
                    {
                        System.IO.File.Delete((webRootPath + extension_old));
                    }

                    using (FileStream fileStream = System.IO.File.Create(webRootPath + "/Upload/" + obj.files.FileName))
                    {
                        obj.files.CopyTo(fileStream);
                        fileStream.Flush();


                        if (obj.files.FileName != null)
                        {
                            newPromotion.image = "/Upload/" + obj.files.FileName;

                        }
                    }
                }
                _context.Update(newPromotion);

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PromotionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
          
          
            return NoContent();
        }

        // POST: api/Promotions
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
     //   [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<Promotions>> PostPromotions([FromForm] FileUploadPromotion obj)
        {
            Promotions newPromotion = new Promotions();
            try
            {
                
                if (obj.files.Length > 0)
                {
                    if (!Directory.Exists(_IWebHostEnvironment.WebRootPath + "/Upload/"))
                    {
                        Directory.CreateDirectory(_IWebHostEnvironment.WebRootPath + "/Upload/");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_IWebHostEnvironment.WebRootPath + "/Upload/" + obj.files.FileName))
                    {
                        obj.files.CopyTo(fileStream);
                        fileStream.Flush();
                        newPromotion.image = "/Upload/" + obj.files.FileName;
                    }
                }
                newPromotion.promotionCode = obj.promotionCode;
                newPromotion.alias = convertToUnSign3(newPromotion.promotionCode);
                newPromotion.discription = obj.discription;
                newPromotion.condition = obj.condition!=null? Int32.Parse(obj.condition):0;
                newPromotion.moneyOff = obj.moneyOff != null ? Int32.Parse(obj.moneyOff) : 0;
                DateTime startTime = DateTime.Parse(obj.startday.ToString("yyyy-MM-dd"));
                DateTime endTime = DateTime.Parse(obj.endDay.ToString("yyyy-MM-dd"));
                //   String.Format("{0:y yy yyy yyyy}", obj.startday);
                newPromotion.startday = startTime.ToString("yyyy-MM-dd");
                newPromotion.endDay = endTime.ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {

                throw ex;
            }

            _context.Promotions.Add(newPromotion);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPromotions", new { id = newPromotion.id }, newPromotion);
        }

        // DELETE: api/Promotions/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<Promotions>> DeletePromotions(int id)
        {
            var promotions = await _context.Promotions.FindAsync(id);
            if (promotions == null)
            {
                return NotFound();
            }
            promotions.delete = true;
            //   _context.Promotions.Remove(promotions);
            _context.Update(promotions);
            await _context.SaveChangesAsync();
        
            return promotions;
        }

        private bool PromotionsExists(int id)
        {
            return _context.Promotions.Any(e => e.id == id);
        }
    }
}
