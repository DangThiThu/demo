﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.Authentication;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypesController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public TypesController(PaymentDetailContext context)
        {
            _context = context;
        }

        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;

            
                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }

        // GET: api/Types
        [HttpGet]

        public async Task<ActionResult<List<Object>>> GetTypes(int page, string name)
        {
            List<Types> typeList = new List<Types>();
            typeList =await _context.Types.Where(a => a.delete != true).ToListAsync();

            if (name != null)
            {
                string keyword = convertToUnSign3(name);
               var newTypeList = from type in _context.Types
                           where type.alias.Contains(keyword)
                           select type;

                typeList = new List<Types>(newTypeList);

            }

            int startIndex = (page - 1) * 15 ;
         
            Object pagination = GetPagiantion(page,typeList.Count());
          


            List<Object> newListType = new List<Object>();
            foreach (var b in typeList)
            {
                List<Types> type = new List<Types>((from types in typeList select types).Skip(startIndex).Take(15));        
                newListType.Add(b.tranform(type, pagination));

                break;

            }
            return newListType;

        }

        // GET: api/Types/5
        [HttpGet("{id}")]
     
        public async Task<ActionResult<Types>> GetTypesDetails(int id)
        {
            var types = await _context.Types.FindAsync(id);

            if (types == null)
            {
                return NotFound();
            }

            return types;
        }

        // PUT: api/Types/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<IActionResult> PutTypes(int id, Types types)
        {
            if (id != types.id)
            {
                return BadRequest();
            }
            types.alias = convertToUnSign3(types.name);

            _context.Entry(types).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Types
      
        [HttpPost]
      //  [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Types>> PostTypes(Types types)
        {
            types.alias = convertToUnSign3(types.name);
            _context.Types.Add(types);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTypes", new { id = types.id }, types);
        }

        // DELETE: api/Types/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Types>> DeleteTypes(int id)
        {
            var types = await _context.Types.FindAsync(id);
            if (types == null)
            {
                return NotFound();
            }
            var product = await _context.Product.Where(a => a.delete != true).SingleOrDefaultAsync(x => x.typeID == id);
            if (product == null)
            {
                types.delete = true;
                _context.Update(types);
                await _context.SaveChangesAsync();


            }
            else
            {
                return null;
            }

           

            // _context.Types.Remove(types);
           

            return types;
        }

        private bool TypesExists(int id)
        {
            return _context.Types.Any(e => e.id == id);
        }
    }
}
