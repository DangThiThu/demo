﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingCartsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public ShoppingCartsController(PaymentDetailContext context)
        {
            _context = context;
        }
        //lấy tất cả danh sách những sản phẩm trong giỏ hàng dựa vào id của khách hàng

        // GET: api/ShoppingCarts/get-all
        [HttpGet]
        [Route("get-all")]
        public async Task<ActionResult<List<JObject>>> GetShoppingCart(int id)
        {
            List<JObject> listJObject = new List<JObject>();
            if (id !=null && id!=0)
            {
                List<ShoppingCart> listShoppingCart = new List<ShoppingCart>(_context.ShoppingCart.Where(b => b.customerID == id));


                foreach (var b in listShoppingCart)
                {

                    var products = _context.Product.Where(product => product.id == b.productID).FirstOrDefault();
                    listJObject.Add(b.tranform(products.image,products.name, products.price,products.percentOff));
                }

            }
            else
            {
                return NotFound();
            }
          
            return listJObject;
        }

        //Cập nhật số lượng của từng món hàng trong giỏ hàng

        // POST: api/ShoppingCarts/update
        [HttpPost]
        [Route("update")]
        public async Task<ActionResult<ShoppingCart>> UpdateShoppingCart(int id, int productID, int soluong)
        {
            var customer  = await _context.Customer.FindAsync(id);
            if (customer==null)
            {
                return NotFound();
            }
            var shoppingCart = _context.ShoppingCart.Where(b => b.customerID == id && b.productID == productID).FirstOrDefault();
            shoppingCart.soluong = soluong;
            _context.Update(shoppingCart);
            await _context.SaveChangesAsync();

            return shoppingCart;
        }

        //them vaò giỏ hàng

        // POST: api/ShoppingCarts
        [HttpPost]
        public async Task<ActionResult<ShoppingCart>> PostShoppingCart(ShoppingCart shoppingCart)
        {
            _context.ShoppingCart.Add(shoppingCart);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShoppingCart", new { id = shoppingCart.id }, shoppingCart);
        }

        //delete sản phẩm trong giỏ hàng

        // POST: api/ShoppingCarts/delete
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult<ShoppingCart>> DeleteProductShoppingCart(int id, int productID)
        {
            var customer = await _context.Customer.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            var shoppingCart = _context.ShoppingCart.Where(b => b.customerID == id && b.productID == productID).FirstOrDefault();
            _context.ShoppingCart.Remove(shoppingCart);
            await _context.SaveChangesAsync();

            return shoppingCart;
        }

        //delete gio hang khi khacs hang nhan thanh toán

        // POST: api/ShoppingCarts/delete-all
        [HttpDelete]
        [Route("delete-all")]
        public async Task<ActionResult<List<ShoppingCart>>> DeleteShoppingCart(int id)
        {
            var customer = await _context.Customer.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            List<ShoppingCart> listShoppingCart = new List<ShoppingCart>(_context.ShoppingCart.Where(b => b.customerID ==id));
            foreach(var temp in listShoppingCart)
            {
                _context.ShoppingCart.Remove(temp);
                await _context.SaveChangesAsync();

            }
        
          

            return listShoppingCart;
        }


    }
}
