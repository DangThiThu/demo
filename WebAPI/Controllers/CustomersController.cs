﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language.Extensions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.Authentication;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public CustomersController(PaymentDetailContext context)
        {
            _context = context;
        }
        //lấy  customer dựa trên email
        // GET: api/Customers
        [HttpGet]
        [Route("get")]
        public async Task<ActionResult<Customer>> GetCustomer(string Email)
        {
            
            return  await _context.Customer.Where(a => a.email == Email).FirstOrDefaultAsync();
        }


        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }
        // get all user base on shopID
        // GET: api/Customers
        [HttpGet]
        [Route("get-shop")]
        public async Task<ActionResult<List<Object>>> GetCustomerByShop(int page)
        {
            var listCustomer = await _context.Customer.ToListAsync();
            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, listCustomer.Count());



            List<Object> newListCustomer = new List<Object>();
            foreach (var b in listCustomer)
            {
                List<Customer> customers = new List<Customer>((from promo in listCustomer select promo).Skip(startIndex).Take(15));
                newListCustomer.Add(b.tranform(customers, pagination));

                break;

            }
            return newListCustomer;


        }

        //// GET: api/Customers/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Customer>> GetCustomer(int id)
        //{
        //    var customer = await _context.Customer.FindAsync(id);

        //    if (customer == null)
        //    {
        //        return NotFound();
        //    }

        //    return customer;
        //}




    }
}
