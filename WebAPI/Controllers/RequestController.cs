﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SQLitePCL;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public RequestController(PaymentDetailContext context)
        {
            _context = context;
        }

        private Object GetPagiantion(int page, int totalTyList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalTyList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalTyList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }


        [HttpGet]
        [Route("get-total")]
        public async Task<ActionResult<List<JObject>>> GetTotalProduct( int page, string search)
        {
            List<JObject> listObject = new List<JObject>();
            List<JObject> listData = new List<JObject>();
            List<Request> listRequest = new List<Request>(_context.Request.Where(a => a.isconfirm ==true));
            listRequest = new List<Request>(listRequest.GroupBy(p => p.productId).Select(grp => grp.FirstOrDefault()));
            if(search != null)
            {
                string keyword = convertToUnSign3(search);
                listRequest = new List<Request>(listRequest.Where(a =>a.alias.Contains(keyword)));
            }
            if(page!=null && page !=0)
            {
                int startIndex = (page - 1) * 15;
                listRequest = new List<Request>(listRequest.Skip(startIndex).Take(15));
            }

            Object pagination = GetPagiantion(page, listRequest.Count());
            foreach (var item in listRequest)
            {
                JObject request = new JObject();
                int amount = 0;
                List<Request> productRequest = new List<Request>(_context.Request.Where(b => b.productId == item.productId));
                foreach(var req in productRequest)
                {
                    amount = amount + req.amount;
                }
                var detailProduct = await _context.Product.FindAsync(item.productId);
                request.Add("productID", JToken.FromObject(item.productId));
                request.Add("detailProduct", JToken.FromObject(detailProduct));
                request.Add("amount", JToken.FromObject(amount));

                listObject.Add(request);

            }

            foreach (var a in listRequest)
            {
                listData.Add(a.tranform(listObject, pagination));
                break;
            }


                return listData;

        }


        [HttpGet]
        [Route("get")]
        [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<List<JObject>>> GetList(int shopID, int page, string search)
        {
            List<JObject> listObject = new List<JObject>();
            List<Request> listRequest = new List<Request>(_context.Request);
            if(shopID!=null && shopID!=0)
            {
                if(search!=null)
                {
                    string keyword = convertToUnSign3(search);
                    listRequest = new List<Request>(listRequest.Where(a => a.shopId == shopID && a.alias.Contains(keyword)));
                }
                else
                {
                    listRequest = new List<Request>(listRequest.Where(a => a.shopId == shopID));
                }
                
            }
              
            foreach (var item in listRequest)
            {
                var product= _context.Product.Where(x=>x.id==item.productId && x.delete ==false).FirstOrDefault();
                var shop = _context.Shops.Where(x => x.id == item.shopId && x.delete == false).FirstOrDefault();
                var numberOfProduct = await _context.NumberOfProduct.Where(a => a.productID == item.productId && a.shopID == item.shopId).FirstOrDefaultAsync();
                JObject request= new JObject();
                request.Add("request", JToken.FromObject(item));
                request.Add("detailProduct", JToken.FromObject(product));
                request.Add("shop", JToken.FromObject(shop));
                request.Add("numberProduct", JToken.FromObject(numberOfProduct.amount));
                listObject.Add(request);
            }

            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, listObject.Count());
            List<JObject> newListReq = new List<JObject>();
            foreach (var b in listObject)
            {
                List<JObject> newData = new List<JObject>((from types in listObject select types).Skip(startIndex).Take(15));
                JObject obj = new JObject();
                obj.Add("data", JToken.FromObject(newData));
                obj.Add("total_page", JToken.FromObject(pagination));
                newListReq.Add(obj);

                break;

            }

            return newListReq;

        }


        //thêm sản phẩm hết hàng cần yêu cầu
        [HttpPost]
        [Route("add")]
       // [Authorize(Roles = "Manager")]
        public async Task<ActionResult<Request>> PostRequest(int shopID, int productID,string name, DateTime date, int amount)
        {
            Request request = new Request();
            request.shopId = shopID;
            request.productId = productID;
            request.name = name;
            request.alias= convertToUnSign3(name);
            request.date = date;
            request.amount = amount;
            request.request = true;
            request.isconfirm = false;
            request.sending = false;
            request.receive = false;
                
            _context.Request.Add(request);
            await _context.SaveChangesAsync();
            return request;


        }



        //Super Admin Cập nhật lại số lượng 
        [HttpPost]
        [Route("update-amount")]
        public async Task<ActionResult<Request>> UpdateAmount (int id, int amount)
        {
            var request = await _context.Request.FindAsync(id);
            request.amount = amount;

            _context.Update(request);

            await _context.SaveChangesAsync();
            return request;

        }


        //câp nhật trạng thái
        [HttpPost]
        [Route("update-state")]
        public async Task<ActionResult<Request>> UpdateState(int id, string state, bool result)
        {
            var request = await _context.Request.FindAsync(id);
            switch (state)
            {
                case "request":
                    request.request = result;
                    break;
                case "isconfirm":
                    request.isconfirm = result;
                   
                    break;
                case "sending":
                    request.sending = result;       
                    break;
                case "receive":

                    request.receive = result;
                    if(result==true)
                    {
                        var numberOfProduct = _context.NumberOfProduct.Where(a => a.shopID == request.shopId && a.productID == request.productId).FirstOrDefault();
                        numberOfProduct.amount = numberOfProduct.amount + request.amount;
                        _context.Update(numberOfProduct);
                        await _context.SaveChangesAsync();

                    }    
                    break;
            
            }
            _context.Update(request);     
            await _context.SaveChangesAsync();
            return request;

        }

        //Xóa yêu cầu
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult<Request>> DeleteRequest(int id)
        {
            var request = await _context.Request.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            _context.Request.Remove(request);
            await _context.SaveChangesAsync();
            return request;
        }
    }
}