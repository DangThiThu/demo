﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.Authentication;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;

        public MaterialsController(PaymentDetailContext context)
        {
            _context = context;
        }
        private Object GetPagiantion(int page, int totalList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }

        // GET: api/Materials
        [HttpGet]
      
        public async Task<ActionResult<List<Object>>> GetMaterialByPage(int page, string name)
        {
            List<Materials> newList = new List<Materials>();
            newList = await _context.Materials.Where(a => a.delete != true).ToListAsync();
            if(name !=null)
            {
                string keyword = convertToUnSign3(name);
                var listMaterial = from mar in _context.Materials
                                   where mar.alias.Contains(keyword)
                                   select mar;

                newList = new List<Materials>(listMaterial);

            }

            int startIndex = (page - 1) * 15;

            Object pagination = GetPagiantion(page, newList.Count());



            List<Object> newListType = new List<Object>();
            foreach (var b in newList)
            {
                List<Materials> newData = new List<Materials>((from types in newList select types).Skip(startIndex).Take(15));
                newListType.Add(b.tranform(newData, pagination));

                break;

            }
            return newListType;
        }

        // GET: api/Materials/5
        [HttpGet("{id}")]
      
        public async Task<ActionResult<Materials>> GetMaterials(int id)
        {
            var materials = await _context.Materials.FindAsync(id);

            if (materials == null)
            {
                return NotFound();
            }

            return materials;
        }

        // PUT: api/Materials/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<IActionResult> PutMaterials(int id, Materials materials)
        {
            if (id != materials.id)
            {
                return BadRequest();
            }
            materials.alias = convertToUnSign3(materials.name);
            _context.Entry(materials).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaterialsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Materials
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        //[Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Materials>> PostMaterials(Materials materials)
        {
            materials.alias = convertToUnSign3(materials.name);
            _context.Materials.Add(materials);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMaterials", new { id = materials.id }, materials);
        }

        // DELETE: api/Materials/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.superAdmin)]
        public async Task<ActionResult<Materials>> DeleteMaterials(int id)
        {
            var materials = await _context.Materials.FindAsync(id);
            if (materials == null)
            {
                return NotFound();

            }

            var product = await _context.Product.Where(a => a.delete != true).SingleOrDefaultAsync(x => x.materialID == id);
            if (product == null)
            {
                materials.delete = true;
                _context.Update(materials);
                await _context.SaveChangesAsync();


            }
            else
            {
                return null;
            }

          

            //  _context.Materials.Remove(materials);
          

            return materials;
        }

        private bool MaterialsExists(int id)
        {
            return _context.Materials.Any(e => e.id == id);
        }
    }
}
