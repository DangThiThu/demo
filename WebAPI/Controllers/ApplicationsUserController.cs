﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.Authentication;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationsUserController : ControllerBase
    {
        private UserManager<ApplicationUser> _userManager;
       // private SignInManager<ApplicationUser> _signInManager;
      //  private readonly ApplicationSettings _appSettings;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;
        private readonly PaymentDetailContext _context;
        private UserManager<Customer> _customer;
     
        







        public ApplicationsUserController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, PaymentDetailContext context)
        {
            _userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;
            _context = context;
        

        }

        private void SendEMail(string emailid, string subject, string body)
        {
            try {
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;


                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("dtttestmail123@gmail.com", "thu260599");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress("dtttestmail123@gmail.com");
                msg.To.Add(new MailAddress(emailid));

                msg.Subject = subject;
                msg.IsBodyHtml = true;
                msg.Body = body;

                
                    client.Send(msg);
                
               
            }
            catch (Exception ex)
            { 
                throw ex;

            }
           
          
        }
        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
        private string GetPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(3, true));
            builder.Append(RandomNumber(100, 999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }


        [Authorize(Roles =UserRoles.superAdmin)]
       // [AllowAnonymous]
        // POST: api/ApplicationsUser/Register
        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> PostApplicationUser([FromBody] ApplicationUserModel model)
        {
           // model.Role = "Admin";
            var userExists = await _userManager.FindByNameAsync(model.userName);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", message = "User already exists!" });
            var applicationUser = new ApplicationUser()
            {
                UserName = model.userName,
                Email = model.email,
                SecurityStamp = Guid.NewGuid().ToString(),
                FullName = model.fullName,
                Delete = false,
                shopID = model.shopID,
                Role = UserRoles.staff,
            };
            try
            {
                var result = await _userManager.CreateAsync(applicationUser, model.password);
               
              //  await roleManager.CreateAsync(new IdentityRole(UserRoles.staff));

                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", message = "User creation failed! Please check user details and try again." });

                if (!await roleManager.RoleExistsAsync(UserRoles.staff))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.staff));

                if (await roleManager.RoleExistsAsync(UserRoles.staff))
                {
                    await _userManager.AddToRoleAsync(applicationUser, UserRoles.staff);
                }

                return Ok(new Response { status = "Success", message = "User created successfully!" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       // [Authorize(Roles = UserRoles.superAdmin)]
        // POST: api/ApplicationsUser/Register-superadmin
        [HttpPost]
        [Route("Register-superadmin")]
        public async Task<IActionResult> RegisterSuperAdmin([FromBody] ApplicationUserModel model)
        {
            // model.Role = "Admin";
            var userExists = await _userManager.FindByNameAsync(model.userName);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", message = "User already exists!" });
            var applicationUser = new ApplicationUser()
            {
                UserName = model.userName,
                Email = model.email,
                SecurityStamp = Guid.NewGuid().ToString(),
                FullName = model.fullName,
                Delete = false,
                Role= UserRoles.superAdmin,
            };
            try
            {
                var result = await _userManager.CreateAsync(applicationUser, model.password);

                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", message = "User creation failed! Please check user details and try again." });

                if (!await roleManager.RoleExistsAsync(UserRoles.superAdmin))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.superAdmin));

                if (await roleManager.RoleExistsAsync(UserRoles.superAdmin))
                {
                    await _userManager.AddToRoleAsync(applicationUser, UserRoles.superAdmin);
                }

                return Ok(new Response { status = "Success", message = "User created successfully!" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         [Authorize(Roles = UserRoles.superAdmin)]
        // POST: api/ApplicationsUser/Register-Manager
        [HttpPost]
        [Route("Register-Manager")]
        public async Task<IActionResult> RegisterManager([FromBody] ApplicationUserModel model)
        {
           
            var userExists = await _userManager.FindByNameAsync(model.userName);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", message = "User already exists!" });
            var applicationUser = new ApplicationUser()
            {
                UserName = model.userName,
                Email = model.email,
                SecurityStamp = Guid.NewGuid().ToString(),
                FullName = model.fullName,
                Delete = false,
                shopID = model.shopID,
                Role = UserRoles.manager,

            };
            try
            {
                var result = await _userManager.CreateAsync(applicationUser, model.password);

                if (!result.Succeeded)
                    return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", message = "User creation failed! Please check user details and try again." });

                if (!await roleManager.RoleExistsAsync(UserRoles.manager))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.manager));

                if (await roleManager.RoleExistsAsync(UserRoles.manager))
                {
                    await _userManager.AddToRoleAsync(applicationUser, UserRoles.manager);
                }

                return Ok(new Response { status = "Success", message = "User created successfully!" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



         [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        // POST: api/ApplicationsUser/Login
        public async Task<IActionResult> Login([FromBody] Login model)
        {
         

            var user = await _userManager.FindByNameAsync(model.username);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.password))
            {
                //get role 
                var role = await _userManager.GetRolesAsync(user); 
                IdentityOptions _options = new IdentityOptions();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID", user.Id.ToString()),
                         new Claim("shopID", user.shopID.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType,role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApplicationSettings:JWT_Secret"])),
                    SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { token });
            }
            else
                return BadRequest(new { message = "Username or password is incorret" });
        }

      //  [AllowAnonymous]
        [Authorize(Roles = "SuperAdmin,Staff,Manager")]
        // POST: api/ApplicationsUser/change-password
        [HttpPost]
        [Route("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePassWord model)
        {
            var user = await _userManager.FindByIdAsync(model.id);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.currentPassword))
            {
                if (model.newPassword != model.confirmNewPassword)
                {
                    Response err = new Response { status = "Fail", message = "New password and confirm new password are different!" };
                    return new ObjectResult(err);
                }
                await _userManager.ChangePasswordAsync(user, model.currentPassword, model.newPassword);
                return Ok(new Response { status = "Success", message = "Change password successfully!" });
            }
            Response response = new Response { status = "Fail", message = "Can't change password now!" };
            return new ObjectResult(response);
        }

        [AllowAnonymous]
       
        //// POST: api/ApplicationsUser/forgetPassWord
        [HttpPost]
        [Route("forgetPassWord")]
        public async Task<IActionResult> ForgetPassWord(string email)
        {
            Response response;
           if (string.IsNullOrEmpty(email))
              return NotFound();
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                Response err = new Response { status = "Fail", message = "No user associated with email!" };
                return new ObjectResult(err);
            }
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var encodedToken = Encoding.UTF8.GetBytes(token);
            var validToken = WebEncoders.Base64UrlEncode(encodedToken);

            var newPassWord = GetPassword();

            string subject = "Password Reset ";
         
            string body = "<h3>New PassWord</h3><br/>" +
                $"<p>Your password is <b>{newPassWord}</b></p>"; //edit it

            try
            {
                var decodedToken = WebEncoders.Base64UrlDecode(validToken);
                string normalToken = Encoding.UTF8.GetString(decodedToken);
                var result = await _userManager.ResetPasswordAsync(user, normalToken, newPassWord);
                SendEMail(email, subject, body);
               
                response = new Response { status = "Success", message = "Reset password URL has been sent to the email successfully!" };

            }
            catch (Exception ex)
            {
                throw ex;
                

            }
            return new ObjectResult(response);
        }
        //// POST: api/ApplicationsUser/LoginSocial
       [AllowAnonymous]
        [HttpPost]
        [Route("LoginSocial")]
        public  async Task<IActionResult> Saveresponse(LoginSocial users)
        {
            try
            {
                Customer Social = new Customer();
                var user = await _context.Customer.Where(a => a.email == users.email).FirstOrDefaultAsync();
                if (user == null)
                {
                    Social.name = users.name;
                    Social.email = users.email;
                    Social.provider = users.provider;
                    _context.Customer.Add(Social);
                    await _context.SaveChangesAsync();

                    IdentityOptions _options = new IdentityOptions();

                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                        new Claim("customerId",Social.id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType,Social.name)
                        }),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApplicationSettings:JWT_Secret"])),
                        SecurityAlgorithms.HmacSha256Signature)
                    };
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                    var token = tokenHandler.WriteToken(securityToken);
                    return Ok(new { token });
                }
                else
                {
                    IdentityOptions _options = new IdentityOptions();

                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                        new Claim("customerId",user.id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType,user.name)
                        }),
                        Expires = DateTime.UtcNow.AddDays(1),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApplicationSettings:JWT_Secret"])),
                        SecurityAlgorithms.HmacSha256Signature)
                    };
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                    var token = tokenHandler.WriteToken(securityToken);
                    return Ok(new { token });
                }

               
            }
            catch (Exception ex)
            {
                throw;
            }
           
        }
        private Object GetPagiantion(int page, int totalList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }


        //Get user
        //// GET: api/ApplicationsUser/GetUser
        [Authorize(Roles = "SuperAdmin,Manager,Staff")]
        [Route("GetUser")]
        [HttpGet]
        public async Task<ActionResult<List<JObject>>> GetApplicationUser(string role, int shopId, int page)
        {
          
            var listUser = await _context.ApplicationsUser.Where(a => a.Delete != true).ToListAsync();
            if (shopId != null && shopId!=0 )
            {
                if (role == "SuperAdmin")
                {
                    listUser = await _context.ApplicationsUser.Where(a => a.Delete != true && a.Role != role && a.shopID == shopId).ToListAsync();
                }
                else if (role == "Manager")
                {
                    listUser = await _context.ApplicationsUser.Where(a => a.Delete != true && a.Role == "Staff" && a.shopID == shopId).ToListAsync();
                }
                else if (role == "Staff")
                {
                    return null;
                }
            }
          //  List<ApplicationUser> newr = new List<ApplicationUser>(listUser);
            int startIndex = (page - 1) * 15;
            List<ApplicationUser> newData = new List<ApplicationUser>((from types in listUser select types).Skip(startIndex).Take(15));
            List<JObject> listJObject = new List<JObject>();
            foreach (var b in newData)
            {
                listJObject.Add(b.tranform());
            }
            Object pagination = GetPagiantion(page, listUser.Count());

            List<JObject> newListObject = new List<JObject>();
            foreach (var b in listUser)
            {

                newListObject.Add(b.tranfromObject(listJObject, pagination));

                break;

            }


            return newListObject;


        }


        //Update user
       [Authorize(Roles = UserRoles.superAdmin)]
        //// POST: api/ApplicationsUser/updateUser
        [HttpPost]
        [Route("updateUser")]
        public async Task<object> UpdateUser(UpdateUser users)
        {
            try
            {
               

                var user = await _userManager.FindByNameAsync(users.userName);
                var roleID = await roleManager.FindByNameAsync(users.role);
                var oldRole= await roleManager.FindByNameAsync(user.Role);
                if (user != null)
                {
                    user.UserName = users.userName;
                    user.FullName = users.fullName;
                    user.Email = users.email;
                  
                    user.Role = users.role;
                    if (users.role != user.Role)
                    {
                        var result = _context.UserRoles.Where(a => a.UserId == user.Id && a.RoleId == oldRole.Id).FirstOrDefault();
                        _context.UserRoles.Remove(result);
                        if (!await roleManager.RoleExistsAsync(users.role))
                            await roleManager.CreateAsync(new IdentityRole(users.role));

                        if (await roleManager.RoleExistsAsync(users.role))
                        {
                            await _userManager.AddToRoleAsync(user, users.role);
                        }
                    }
          
                    _context.Update(user);
                
                    await _context.SaveChangesAsync();
                    return new Response
                    {
                        status = "Success",
                        message = "Update Success!"
                    };

                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return new Response
            {
                status = "False",
                message = "Update False!"
            };

        }
        [Authorize(Roles = UserRoles.superAdmin)]
        //// POST: api/ApplicationsUser/deleteUser
        [HttpDelete]
        [Route("deleteUser")]
        public async Task<object> DeleteUser(string userName)
        {
            try
            {

                var user = await _userManager.FindByNameAsync(userName);
                if (user != null)
                {
                    user.Delete = true;
                    user.LockoutEnd = DateTime.Now.AddYears(1000);

                    _context.Update(user);
                    await _context.SaveChangesAsync();
                    return new Response
                    {
                        status = "Success",
                        message = "Update Success!"
                    };

                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return new Response
            {
                status = "False",
                message = "Update False!"
            };

        }




    }
}

