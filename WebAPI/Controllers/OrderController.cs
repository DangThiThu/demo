﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stripe.Checkout;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Models.Authentication;
using WebAPI.Models.ViewModel;
using Order = WebAPI.Models.Order;
using Customer = WebAPI.Models.Customer;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly PaymentDetailContext _context;
        public static IWebHostEnvironment _IWebHostEnvironment;

        public OrderController(IWebHostEnvironment iWebHostEnvironment, PaymentDetailContext context)
        {
            _IWebHostEnvironment = iWebHostEnvironment;
            _context = context;
        }

        private Object GetPagiantion(int page, int totalList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page + 1 : 0;
                int total = totalList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }

        // lấy danh sách order dựa theo số điện thoại của khách hàng
        [HttpGet]
        [Authorize(Roles = "SuperAdmin,Manager,Staff")]
        [Route("search-by-phone")]
        public async Task<ActionResult<JObject>> SearchOrderByPhone(string phone)
        {
            var order = _context.Order.Where(a => a.delete != true && a.customerPhoneNumber == phone);
            List<Order> orders = new List<Order>(order);
            JObject newOrder = new JObject();
            foreach (var or in orders)
            {
                List<OrderDetail> listOrderDetail = new List<OrderDetail>();
                listOrderDetail = await _context.OrderDetail.Where(a => a.delete != true && a.orderId == or.id).ToListAsync();

                var namePerson = await _context.ApplicationsUser.Where(a => a.Delete != true && a.Id == or.salePersonId).FirstOrDefaultAsync();
                string name = "";
                if (namePerson != null)
                {
                    name = namePerson.FullName;
                }
                newOrder.Add(or.tranform(name, listOrderDetail, null));

            }
            return newOrder;
        }

        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }


        //phân trang danh sách Order
        // GET: api/Order
        [HttpGet]
        [Authorize(Roles = "SuperAdmin,Manager,Staff")]
        public async Task<ActionResult<List<JObject>>> GetOrders(int orderID, string userID, int page, string name, DateTime startDate, DateTime endDate)
        {
            int startIndex = (page - 1) * 15;
            var listOrder = new List<Order>();
            if (orderID != null && orderID != 0)
            {
                listOrder = await _context.Order.Where(a => a.delete != true && a.id == orderID).ToListAsync();
            }
            else if (orderID == null || orderID == 0)
            {
                listOrder = await _context.Order.Where(a => a.delete != true).ToListAsync();
            }
            if (userID != null)
            {
                var userRole = await _context.ApplicationsUser.Where(b => b.Delete != true && b.Id == userID).FirstOrDefaultAsync();
                if (userRole.Role == "SuperAdmin" || userRole.Role == "Manager")
                {
                    if (userRole.Role == "SuperAdmin")
                    {
                        listOrder = await _context.Order.Where(a => a.delete != true).ToListAsync();
                    }
                    else
                    {
                        listOrder = await _context.Order.Where(a => a.delete != true && a.shopID == userRole.shopID).ToListAsync();
                    }
                }
                else if (userRole.Role == "Staff")
                {
                    listOrder = await _context.Order.Where(a => a.delete != true && a.salePersonId == userID).ToListAsync();
                }

            }
            if (name != null)
            {
                string keyword = convertToUnSign3(name);
                listOrder = (List<Order>)listOrder.Where(a => a.customerPhoneNumber.Contains(keyword) || a.customerName.Contains(name)).ToList();
            }

            DateTime defaultDate = new DateTime(0);
            int a = DateTime.Compare(startDate, defaultDate);
            if (DateTime.Compare(startDate, defaultDate) != 0 && DateTime.Compare(endDate, defaultDate) != 0)
            {
                startDate = startDate.Date.AddHours(0).AddMinutes(0).AddSeconds(1);
                endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                listOrder = (List<Order>)listOrder.Where(a => DateTime.Compare(a.appointment, startDate) >= 0 && DateTime.Compare(a.appointment, endDate) <= 0).ToList();

            }
            Object pagination = GetPagiantion(page, listOrder.Count());
            List<Order> newData = new List<Order>(from order in listOrder select order);
            if (page != 0)
            {
                newData = new List<Order>((from order in listOrder select order).Skip(startIndex).Take(15));
            }

            List<JObject> newListOrder = new List<JObject>();
            foreach (var b in newData)
            {
                List<OrderDetail> listOrderDetail = new List<OrderDetail>();
                listOrderDetail = await _context.OrderDetail.Where(a => a.delete != true && a.orderId == b.id).ToListAsync();

                var namePerson = await _context.ApplicationsUser.Where(a => a.Delete != true && a.Id == b.salePersonId).FirstOrDefaultAsync();
                string customerName = "";
                if (namePerson != null)
                {
                    customerName = namePerson.FullName;
                }
                var Promotion = await _context.Promotions.FindAsync(b.promotionID);

                newListOrder.Add(b.tranform(customerName, listOrderDetail, Promotion));
            }


            List<JObject> newListObject = new List<JObject>();
            foreach (var b in newData)
            {

                newListObject.Add(b.tranfromObject(newListOrder, pagination));

                break;

            }
            return newListObject;


        }



        //Xác nhận đơn hàng được đặt
        //API: api/Order/confirm
        [HttpPost]
        [Authorize(Roles = "SuperAdmin,Manager,Staff")]
        [Route("confirm")]
        public async Task<IActionResult> ConfirmOrder(int id, string salePersonID, string appoitment, string confirm)
        {
            try
            {
                var order = await _context.Order.Where(a => a.delete != true && a.id == id).FirstOrDefaultAsync();
                order.isConfirmed = Boolean.Parse(confirm);
                order.appointment = DateTime.Parse(appoitment);
                var namePerson = await _context.ApplicationsUser.Where(a => a.Delete != true && a.Id == salePersonID).FirstOrDefaultAsync();
                if (namePerson != null)
                {
                    order.salePersonId = salePersonID;
                    order.nameSalePerson = namePerson.FullName;
                    _context.Update(order);
                }
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return NoContent();
        }

        //Hoàn tất đơn hàng khi giao một là thành công hai là bùng hàng check lại số lượng product còn lại
        [HttpPost]
        [Authorize(Roles = "SuperAdmin,Manager")]
        [Route("completed")]
        public async Task<IActionResult> CompletedOrder(int orderId)
        {
            try
            {
                var order = await _context.Order.Where(a => a.delete != true && a.id == orderId).FirstOrDefaultAsync();
                order.isCompleted = true;
                order.isPayment = true;
                _context.Update(order);
                await _context.SaveChangesAsync();

                return Ok(new Response { status = "Success", message = "Order is completed!" });


            }
            catch (Exception ex)
            {
                throw ex;
            }


            return NoContent();

        }



        // Post: api/Orders
        private async Task<ActionResult> AddOrderDetail(int orderID, OrderDetailVM[] listProducts)
        {

            for (int i = 0; i < listProducts.Length; i++)
            {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.orderId = orderID;
                orderDetail.productID = listProducts[i].id;
                orderDetail.name = listProducts[i].name;
                orderDetail.image = listProducts[i].imUrl;
                orderDetail.price = listProducts[i].price;
                orderDetail.quantityOfProduct = listProducts[i].qty;
                _context.OrderDetail.Add(orderDetail);
                await _context.SaveChangesAsync();


            }
            return Ok(new Response { status = "Success", message = "OrderDetail created successfully!" });


        }

        //kiểm tra số lượng của sản phấm sau khi đặt hàng xong
        private async void checkQuantityOfProduct(int id, int soluong, int shopID)
        {
            var numberOfProduct = _context.NumberOfProduct.Where(a => a.productID == id).FirstOrDefault();
            int newAmount = numberOfProduct.amount - soluong;
            numberOfProduct.amount = newAmount;
            _context.Update(numberOfProduct);
            await _context.SaveChangesAsync();

        }

        //Update thông tin trong order

        [HttpPost]
        [Authorize(Roles = "SuperAdmin,Manager")]
        [Route("update")]
        public async Task<ActionResult<Order>> UpdateOrder(int orderId, string appointment, bool isConfirm, bool isCompleted)
        {
            if (orderId == null || orderId == 0)
            {
                return NoContent();
            }
            var order = _context.Order.Where(a => a.delete != true && a.id == orderId).FirstOrDefault();
            order.appointment = DateTime.Parse(appointment);
            order.isCompleted = isCompleted;
            order.isConfirmed = isConfirm;
            _context.Update(order);
            await _context.SaveChangesAsync();
            return order;



        }



        // POST: api/Order
        [HttpPost]
        [Route("create-order")]
        public async Task<ActionResult<Order>> PostOrder(OrderVM orderVM)
        {
            try
            {
                //lưu đơn hàng vào data
                Order order = new Order();
                order.customerName = orderVM.name;
                order.customerPhoneNumber = orderVM.phoneNumber;
                order.customerEmail = orderVM.email;
                order.appointment = DateTime.Parse(orderVM.appointment);
                order.address = orderVM.address;

                if (orderVM.shopID != null)
                {

                    order.shopID = orderVM.shopID;
                }
                else
                {
                    return Ok(new Response { status = "Fail", message = "Order must be have shopID!" });
                }




                if (orderVM.customerInfo != null)
                {
                    string customerEmail = orderVM.customerInfo.email;
                    var customer = await _context.Customer.Where(a => a.email == customerEmail).FirstOrDefaultAsync();
                    if (customer != null)
                    {
                        customer.address = orderVM.address;
                        customer.fullname = orderVM.name;
                        customer.phone = orderVM.phoneNumber;
                        _context.Update(customer);
                        await _context.SaveChangesAsync();
                    }


                }
                else
                {
                    Customer CustomerInFo = new Customer();
                    CustomerInFo.name = orderVM.name;
                    CustomerInFo.email = orderVM.email;
                    CustomerInFo.fullname = orderVM.name;
                    CustomerInFo.address = orderVM.address;
                    CustomerInFo.phone = orderVM.phoneNumber;
                    _context.Customer.Add(CustomerInFo);
                    await _context.SaveChangesAsync();
                }

                var promotion = await _context.Promotions.Where(a => a.id ==Int32.Parse(orderVM.promotionCode) && a.delete != true).FirstOrDefaultAsync();
                if (orderVM.promotionCode != null)
                {

                    order.promotionID = promotion.id != null ? promotion.id : 0;
                }

                int realMoney = 0;

                for (int i = 0; i < orderVM.listProducts.Length; i++)
                {
                    realMoney = realMoney + orderVM.listProducts[i].qty * orderVM.listProducts[i].price;

                }
                order.realmoney = realMoney;
                int moneyIsReduced = 0;

                //  var promotion = await _context.Promotions.Where(a => a.id == promotionID && a.delete != true).FirstOrDefaultAsync();

                if (orderVM.promotionCode != null && promotion != null)
                {

                    DateTime startTime = DateTime.Parse(promotion.startday.ToString());
                    DateTime endTime = DateTime.Parse(promotion.endDay.ToString());
                    DateTime appoiment = order.appointment;
                    if (endTime.Date >= appoiment.Date && startTime.Date <= appoiment.Date)
                    {
                        moneyIsReduced = promotion.moneyOff;
                    }
                }

                order.moneyIsReduced = moneyIsReduced;
                order.totalmoney = realMoney - moneyIsReduced;

                _context.Order.Add(order);
                await _context.SaveChangesAsync();
                int orderID = order.id;

                for (int k = 0; k < orderVM.listProducts.Length; k++)
                {
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.orderId = orderID;
                    orderDetail.productID = orderVM.listProducts[k].id;
                    orderDetail.name = orderVM.listProducts[k].name;
                    orderDetail.image = orderVM.listProducts[k].imUrl;
                    orderDetail.price = orderVM.listProducts[k].price;
                    orderDetail.quantityOfProduct = orderVM.listProducts[k].qty;


                    //giam so luong cua san pham da bán
                    var numberOfProduct = _context.NumberOfProduct.Where(a => a.productID == orderVM.listProducts[k].id && a.shopID == orderVM.shopID).FirstOrDefault();
                    int newAmount = numberOfProduct.amount - orderVM.listProducts[k].qty;
                    numberOfProduct.amount = newAmount;
                    _context.Update(numberOfProduct);
                    await _context.SaveChangesAsync();
                    // checkQuantityOfProduct(orderVM.listProducts[k].id, orderVM.listProducts[k].qty, orderVM.shopID);

                    _context.OrderDetail.Add(orderDetail);
                    await _context.SaveChangesAsync();



                }
                return order;
            }
            catch (Exception ex)
            {
                throw ex;

            }

        }









        // POST: api/Order
        [HttpPost("create-checkout-session")]
        public async Task<ActionResult> CreateCheckouteSession(OrderVM orderVM)
        {
            try
            {
               
                //lưu đơn hàng vào data
                Order order = new Order();
                order.customerName = orderVM.name;
                order.customerPhoneNumber = orderVM.phoneNumber;
                order.customerEmail = orderVM.email;
                order.appointment = DateTime.Parse(orderVM.appointment);
                order.address = orderVM.address;
                order.delete = true;
                
                if (orderVM.shopID != null)
                {

                    order.shopID = orderVM.shopID;
                }
                else
                {
                    return Ok(new Response { status = "Fail", message = "Order must be have shopID!" });
                }




                if (orderVM.customerInfo != null)
                {
                    string customerEmail = orderVM.customerInfo.email;
                    var customer = await _context.Customer.Where(a => a.email == customerEmail).FirstOrDefaultAsync();
                    if (customer != null)
                    {
                        customer.address = orderVM.address;
                        customer.fullname = orderVM.name;
                        customer.phone = orderVM.phoneNumber;
                        _context.Update(customer);
                        await _context.SaveChangesAsync();
                    }


                }
                else
                {
                    Customer CustomerInFo = new Customer();
                    CustomerInFo.name = orderVM.name;
                    CustomerInFo.email = orderVM.email;
                    CustomerInFo.fullname = orderVM.name;
                    CustomerInFo.address = orderVM.address;
                    CustomerInFo.phone = orderVM.phoneNumber;
                    _context.Customer.Add(CustomerInFo);
                    await _context.SaveChangesAsync();
                }
                string promotionCODE = orderVM.promotionCode;
                DateTime appoiment = order.appointment;
                

                int realMoney = 0;

                for (int i = 0; i < orderVM.listProducts.Length; i++)
                {
                    realMoney = realMoney + orderVM.listProducts[i].qty * orderVM.listProducts[i].price;

                }
                order.realmoney = realMoney;
                int moneyIsReduced = 0;

                //  var promotion = await _context.Promotions.Where(a => a.id == promotionID && a.delete != true).FirstOrDefaultAsync();
                var promotion = await _context.Promotions.Where(a => a.id == Int32.Parse(orderVM.promotionCode) && a.delete != true).FirstOrDefaultAsync();

                if (orderVM.promotionCode != null && promotion != null)
                {
                    moneyIsReduced = promotion.moneyOff;
                    order.promotionID = promotion.id;
                }

                order.moneyIsReduced = moneyIsReduced;
                order.totalmoney = realMoney - moneyIsReduced;

                _context.Order.Add(order);
                await _context.SaveChangesAsync();
                int orderID = order.id;

                for (int k = 0; k < orderVM.listProducts.Length; k++)
                {
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.orderId = orderID;
                    orderDetail.productID = orderVM.listProducts[k].id;
                    orderDetail.name = orderVM.listProducts[k].name;
                    orderDetail.image = orderVM.listProducts[k].imUrl;
                    orderDetail.price = orderVM.listProducts[k].price;
                    orderDetail.quantityOfProduct = orderVM.listProducts[k].qty;


                    //giam so luong cua san pham da bán
                    var numberOfProduct = _context.NumberOfProduct.Where(a => a.productID == orderVM.listProducts[k].id && a.shopID == orderVM.shopID).FirstOrDefault();
                    int newAmount = numberOfProduct.amount - orderVM.listProducts[k].qty;
                    numberOfProduct.amount = newAmount;
                    _context.Update(numberOfProduct);
                    await _context.SaveChangesAsync();
                    // checkQuantityOfProduct(orderVM.listProducts[k].id, orderVM.listProducts[k].qty, orderVM.shopID);

                    _context.OrderDetail.Add(orderDetail);
                    await _context.SaveChangesAsync();


                }



                // create session checkout
                List<SessionLineItemOptions> newLineItems = new List<SessionLineItemOptions>();
                for (int k = 0; k < orderVM.listProducts.Length; k++)
                {
                    SessionLineItemOptions item = new SessionLineItemOptions
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            UnitAmount = orderVM.listProducts[k].price,
                            Currency = "vnd",
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = orderVM.listProducts[k].name,
                            },
                        },
                        Quantity = orderVM.listProducts[k].qty,

                    };

                    newLineItems.Add(item);


                }
                if (order.moneyIsReduced != 0)
                {
                    var couponOptions = new CouponCreateOptions
                    {
                        AmountOff = order.moneyIsReduced,
                        Currency = "vnd",
                    };
                    var couponService = new CouponService();
                    Coupon coupon = couponService.Create(couponOptions);
                    var options = new SessionCreateOptions
                    {
                        PaymentMethodTypes = new List<string>
                {
                  "card",
                },
                        LineItems = newLineItems,
                        Mode = "payment",
                        Discounts = new List<SessionDiscountOptions>
                  {
                    new SessionDiscountOptions
                    {
                      Coupon = coupon.Id,
                    },
                  },
                        SuccessUrl = "https://deploy-angular-2afbb.web.app/checkout/success/" + order.id,
                        CancelUrl = "https://deploy-angular-2afbb.web.app/checkout/fail/" + +order.id,
                    };
                    var service = new SessionService();
                    Session session = service.Create(options);
                    return new ObjectResult(session) { StatusCode = 200 };


                }
                else
                {
                    var options = new SessionCreateOptions
                    {
                        PaymentMethodTypes = new List<string>
                {
                  "card",
                },
                        LineItems = newLineItems,
                        Mode = "payment",

                        SuccessUrl = "https://deploy-angular-2afbb.web.app/checkout/success/" + order.id,
                        CancelUrl = "https://deploy-angular-2afbb.web.app/checkout/fail/" + +order.id,
                    };
                    var service = new SessionService();
                    Session session = service.Create(options);
                    return new ObjectResult(session) { StatusCode = 200 };

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }





        //Sau khi thanh toan thanh cong
        [HttpPost]
        [Route("completed-payment")]

        public async Task<ActionResult<Order>> CompletedPaymentAfterPayment(string orderID)
        {
            int orderid;
            Int32.TryParse(orderID, out orderid);
            if (orderID == null || orderid == 0)
            {
                return NoContent();
            }
            var order = _context.Order.Where(a => a.id == orderid).FirstOrDefault();
            order.isPayment = true;
            order.delete = false;
            _context.Update(order);
            await _context.SaveChangesAsync();
            return order;

        }





        //Xóa vật lý đơn hàng
        [HttpDelete]
        [Route("delete")]
        public async Task<ActionResult<Order>> DeleteOrder(int id)
        {
            //Xoa order da khong not completed
            var orders = await _context.Order.Where(a => a.id == id).FirstOrDefaultAsync();
            if (orders.delete == true && orders.isPayment == false)
            {
                var listOrderDetail = await _context.OrderDetail.Where(a => a.delete != true && a.orderId == id).ToListAsync();
                foreach (var a in listOrderDetail)
                {
                    _context.OrderDetail.Remove(a);
                    //kiểm tra lại số lượng sản phẩm khi đơn hàng thanh toán k thành công
                    var numberOfProduct = _context.NumberOfProduct.Where(nu => nu.shopID == orders.shopID && nu.productID == a.productID).FirstOrDefault();
                    if (numberOfProduct != null)
                    {
                        numberOfProduct.amount = numberOfProduct.amount + a.quantityOfProduct;
                        _context.Update(numberOfProduct);
                        await _context.SaveChangesAsync();
                    }

                }
                _context.Order.Remove(orders);
                await _context.SaveChangesAsync();
            }
            return orders;
        }


        // DELETE: api/Order/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<Order>> DeleteOrders(int id)
        {
            //Xoa order da khong not completed
            var orders = await _context.Order.Where(a => a.delete != true && a.id == id && a.isCompleted == false && a.isPayment == false).FirstOrDefaultAsync();
            if (orders == null)
            {
                return Ok(new Response { status = "Error", message = "Order is completed!" });
            }

            var listOrderDetail = await _context.OrderDetail.Where(a => a.delete != true && a.orderId == id).ToListAsync();
            foreach (var a in listOrderDetail)
            {
                a.delete = true;
                _context.Update(a);
                await _context.SaveChangesAsync();
                //kiểm tra lại số lượng sản phẩm khi bị hủy đơn hàng
                var numberOfProduct = _context.NumberOfProduct.Where(nu => nu.shopID == orders.shopID && nu.productID == a.productID).FirstOrDefault();
                if (numberOfProduct != null)
                {
                    numberOfProduct.amount = numberOfProduct.amount + a.quantityOfProduct;
                    _context.Update(numberOfProduct);
                    await _context.SaveChangesAsync();
                }

            }
            orders.delete = true;
            _context.Update(orders);
            await _context.SaveChangesAsync();
            return orders;


        }


        //khách hàng hủy đơn hàng
        // DELETE: api/Order/cancel-order
        [HttpPost]
        [Route("cancel-order")]

        public async Task<ActionResult<Order>> CancelOrderByCustom(int id, string email)
        {
            //lấy order theo id
            var order = await _context.Order.Where(a => a.delete != true && a.id == id && a.customerEmail == email).FirstOrDefaultAsync();
            order.cancel = true;

            _context.Update(order);
            await _context.SaveChangesAsync();
            return order;


        }


        // get danh sách order theo email cả khách hàng
        [HttpGet]
        [Route("get-by-email")]
        public async Task<ActionResult<List<Object>>> ListOrderByEmail(string email)
        {
            List<Object> listOjectOrder = new List<Object>();
            List<Order> listOrder = new List<Order>();
            listOrder = await _context.Order.Where(a => a.delete != true && a.customerEmail == email).ToListAsync();

            foreach (var order in listOrder)
            {
                var orderDetails = await _context.OrderDetail.Where(or => or.orderId == order.id).ToListAsync();
                JObject obj = new JObject();
                obj.Add("order", JToken.FromObject(order));
                obj.Add("orderDetail", JToken.FromObject(orderDetails));
                listOjectOrder.Add(obj);
            }
            return listOjectOrder;


        }

        //Cập nhật trạng thái của đơn hàng

        [HttpPost]
        [Route("update-state")]
        [Authorize(Roles = "SuperAdmin,Manager,Staff")]

        public async Task<ActionResult<Order>> updateStateOrder(int orderID, string state, bool result)
        {
            var order = await _context.Order.Where(a => a.delete != true && a.id == orderID).FirstOrDefaultAsync();

            switch (state)
            {
                case "isConfirmed":
                    order.isConfirmed = result;
                    break;
                case "delivering":
                    order.isConfirmed = result;
                    order.delivering = result;
                    break;
                case "success_deliver":
                    if (order.isPayment)
                    {
                        order.isCompleted = result;
                    }
                    order.isConfirmed = result;
                    order.delivering = result;
                    order.success_deliver = result;
                    break;
                case "isPayment":
                    if (order.success_deliver)
                    {
                        order.isCompleted = result;
                    }
                    order.isPayment = result;
                    break;
                case "cancel":
                    order.isConfirmed = !result;
                    order.cancel = result;
                    break;
                case "isCompleted":
                    order.isConfirmed = result;
                    order.delivering = result;
                    order.success_deliver = result;
                    order.isCompleted = result;
                    break;
                case "delete":
                    order.delete = result;
                    order.isConfirmed = !result;
                    order.delivering = !result;
                    order.success_deliver = !result;
                    order.isCompleted = !result;
                    break;
            }
            _context.Update(order);
            await _context.SaveChangesAsync();
            return order;
        }




    }
}