﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace WebAPI.Controllers
{
    public class ReCaptchaResponse
    {
        public bool Success;
        public string ChallengeTs;
        public string Hostname;
        public object[] ErrorCodes;
    }

    public class ReCaptchaToken
    {
        public string token;
    }

    [Route("api/[controller]")]
    [ApiController]
    public class RecaptraController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public RecaptraController( IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost]
        public bool Captcha([FromBody] ReCaptchaToken body)
        {
            bool isHuman = true;
            try
            {
                string secretKey = _configuration["reCaptchaPrivateKey"];
                Uri uri = new Uri("https://www.google.com/recaptcha/api/siteverify" +
                                  $"?secret={secretKey}&response={body.token}");
                HttpWebRequest request = WebRequest.CreateHttp(uri);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = 0;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream);
                string result = streamReader.ReadToEnd();
                ReCaptchaResponse reCaptchaResponse = JsonConvert.DeserializeObject<ReCaptchaResponse>(result);
                isHuman = reCaptchaResponse.Success;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("reCaptcha error: " + ex);
            }

            return isHuman;
        }
    }
}
