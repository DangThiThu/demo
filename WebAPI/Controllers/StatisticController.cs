﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.Language;
using Microsoft.AspNetCore.Routing;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Models.Authentication;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticController : ControllerBase
    {
        private readonly PaymentDetailContext _context;
        public static IWebHostEnvironment _IWebHostEnvironment;
        public StatisticController(IWebHostEnvironment iWebHostEnvironment, PaymentDetailContext context)
        {
            _IWebHostEnvironment = iWebHostEnvironment;
            _context = context;
        }

       //thoonsgs kê tỉ trọng sản phẩm bán được theo ngày
        [HttpGet]
        [Route("statistic-order")]
        [Authorize(Roles = "SuperAdmin,Manager,Staff")]
        public async Task<ActionResult<List<JObject>>> StatisticOrder(string startDay, string endDay,int shopId)
        {
            List<Object> listProduct = new List<Object>();
            List<NewProduct> listNewProduct = new List<NewProduct>();
            List<JObject> listObject = new List<JObject>();
            DateTime startDate = DateTime.Parse(startDay.ToString());
            DateTime endDate = DateTime.Parse(endDay.ToString());
            double tongsp = 0;

            //lay danh sach order theo ngay
            List<Order> listOrder2 = new List<Order>();
            startDate = startDate.Date.AddHours(0).AddMinutes(0).AddSeconds(1);
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            if (shopId!=null && shopId!=0)
            {
                listOrder2 = new List<Order>(_context.Order.Where(a => DateTime.Compare(a.appointment, startDate) >= 0 && DateTime.Compare(a.appointment, endDate) <= 0 && a.shopID==shopId && a.delete != true));
            }
            else
            {
                listOrder2 = new List<Order>(_context.Order.Where(a => DateTime.Compare(a.appointment, startDate) >= 0 && DateTime.Compare(a.appointment, endDate) <= 0 && a.delete != true));
            }
            //C2
            //list order detail theo orderid phia tren
            List<OrderDetail> newListOrderDetail = new List<OrderDetail>(_context.OrderDetail.Where(b=>b.delete!=true));
            List<OrderDetail> lisOrderDetailByOrder = new List<OrderDetail>();
            foreach(var order in listOrder2)
            {

                List <OrderDetail> newOrderDetail = new List<OrderDetail>(newListOrderDetail.Where(c => c.orderId == order.id));
                foreach (var oneOrderDetail in newOrderDetail)
                {
                    lisOrderDetailByOrder.Add(oneOrderDetail);
                    tongsp = tongsp + oneOrderDetail.quantityOfProduct;
                }
            }
            //lay duy nhat nhung productID bi trung trong listorderDetail
            var listproductID= lisOrderDetailByOrder.GroupBy(p => p.productID).Select(g => g.First()).ToList();
            foreach(var productId in listproductID)
            {
                List<OrderDetail> listSameProductID = new List<OrderDetail>(lisOrderDetailByOrder.Where(a => a.productID == productId.productID));
                //tong cua tung product trong list OrderDetail
                double tongmotsp = 0;
                foreach(var item in listSameProductID)
                {
                    tongmotsp = tongmotsp + item.quantityOfProduct;
                }
                var product = _context.Product.Where(a => a.id == productId.productID && a.delete != true).FirstOrDefault();
                double phantram = (double)tongmotsp / (double)tongsp * 100; 
                JObject obj = new JObject();
               // obj.Add("productID", productId.productID);
                obj.Add("name", product.name);
                obj.Add("percent", phantram);

                listObject.Add(obj);
            }


            return listObject;

        }





        //Thống kê đơn hàng theo ngày
        [HttpGet]
        [Route("statistic-number-order")]
        public async Task<ActionResult<List<int>>> StatictisOrderByDate(string startDay, string endDay, int shopId)
        {
            DateTime startDate = DateTime.Parse(startDay.ToString());
            DateTime endDate = DateTime.Parse(endDay.ToString());
            startDate = startDate.Date.AddHours(0).AddMinutes(0).AddSeconds(1);
            endDate = endDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            List<int> ListNumberOrder = new List<int>();
            if (DateTime.Compare(startDate, endDate) == 0)
            {
                var listOrder = await _context.Order.Where(a => a.isCompleted == true && startDate.Day == a.appointment.Day && startDate.Month == a.appointment.Month && startDate.Year == a.appointment.Year && a.delete != true).ToListAsync();
                if (shopId != null && shopId != 0)
                {
                    listOrder = await _context.Order.Where(a => a.isCompleted == true && startDate.Day == a.appointment.Day && startDate.Month == a.appointment.Month && startDate.Year == a.appointment.Year && a.shopID == shopId && a.delete != true).ToListAsync();
                }
                ListNumberOrder.Add(listOrder.Count);
            }
            else
            {
                var listOrder = await _context.Order.Where(a => a.isCompleted == true && startDate.Day == a.appointment.Day && startDate.Month == a.appointment.Month && startDate.Year == a.appointment.Year ).ToListAsync();
                if (shopId != null && shopId != 0)
                {
                    listOrder = await _context.Order.Where(a => a.isCompleted == true && startDate.Day == a.appointment.Day && startDate.Month == a.appointment.Month && startDate.Year == a.appointment.Year && a.shopID == shopId && a.delete != true).ToListAsync();
                }
                //   List<Order> listOrder = new List<Order>();
                ListNumberOrder.Add(listOrder.Count);

                DateTime date = startDate.AddDays(1);
                while (DateTime.Compare(date, endDate)<=0)
                {
                    List<Order> order = new List<Order>(_context.Order.Where(a => a.isCompleted == true && date.Day == a.appointment.Day && date.Month == a.appointment.Month && date.Year == a.appointment.Year && a.delete != true));
                    if (shopId != null && shopId != 0)
                    {
                        order = await _context.Order.Where(a => a.isCompleted == true && date.Day == a.appointment.Day && date.Month == a.appointment.Month && date.Year == a.appointment.Year && a.shopID == shopId && a.delete != true).ToListAsync();
                    }
                    ListNumberOrder.Add(order.Count);
                    date = date.AddDays(1);
                }    

            }

            return ListNumberOrder;


        }






    }

}