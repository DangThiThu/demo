﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Connections;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using WebAPI.Models;
using WebAPI.Models.ViewModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly PaymentDetailContext _context;
        public static IWebHostEnvironment _IWebHostEnvironment;

        public ProductsController(IWebHostEnvironment iWebHostEnvironment, PaymentDetailContext context)
        {
            _IWebHostEnvironment = iWebHostEnvironment;
            _context = context;
        }
        // GET: api/Products/getPercentOff
        [HttpGet]
        [Route("getPercentOff")]
       
        public async Task<ActionResult<List<JObject>>> GetAllPercentOff(int page)
        {
            int startIndex = (page - 1) * 15;
            List<Product> listProduct = new List<Product>();

            listProduct = await _context.Product.Where(a => a.delete != true && a.percentOff>0).ToListAsync();

            List<Product> newData = new List<Product>((from types in listProduct select types).Skip(startIndex).Take(15));

            List<JObject> listJObject = new List<JObject>();
            foreach (var b in newData)
            {
                listJObject.Add(b.tranform());
            }
            Object pagination = GetPagiantion(page, listProduct.Count());

            List<JObject> newListObject = new List<JObject>();
            foreach (var b in listProduct)
            {

                newListObject.Add(b.tranfromObject(listJObject, pagination));

                break;

            }

            return newListObject;
        }
        // GET: api/Products/newProduct
        [HttpGet]
        [Route("newProduct")]
       
        public async Task<ActionResult<List<JObject>>> GetAllNewProduct(int page)
        {
            int startIndex = (page - 1) * 15;
            List<Product> listProduct = new List<Product>();

            listProduct = await _context.Product.Where(a => a.delete != true).ToListAsync();
            listProduct = listProduct.OrderByDescending(a => a.id).ToList();
            List<Product> newData = new List<Product>((from types in listProduct select types).Skip(startIndex).Take(15));
         
            List<JObject> listJObject = new List<JObject>();
            foreach (var b in newData)
            {
                listJObject.Add(b.tranform());
            }

            Object pagination = GetPagiantion(page, listProduct.Count());

            List<JObject> newListObject = new List<JObject>();
            foreach (var b in listProduct)
            {

                newListObject.Add(b.tranfromObject(listJObject, pagination));

                break;

            }

            return newListObject;
        }
        //ham thuc hien chuc nang chuyen doi ve dang chu thuong
        public static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').ToLower();
        }
        private Object GetPagiantion(int page, int totalList)
        {
            JObject pagination = new JObject();
            if (page != null && page != 0)
            {
                int startIndex = (page - 1) * 15 + 1;
                int stopIndex = page * 15;
                int currentPage = page;
                int checkNextPage = totalList - stopIndex;
                int nextPage = checkNextPage >= 0 ? page+1 : 0;
                int total = totalList;


                pagination.Add("currentPage", currentPage);
                pagination.Add("nextPage", nextPage);
                pagination.Add("total", total);

            }
            return pagination;
        }
       private async void AddItemNumberOfProduct(int productId,int soluong)
        {
            try
            {
                List<NumberOfProduct> numberofproduct = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.productID == productId));
                 if (numberofproduct.Count<1)
                {
                    List<Shops> listShop = new List<Shops>(_context.Shops.Where(item => item.delete != true));
                    foreach (var shop in listShop)
                    {
                        NumberOfProduct newProduct = new NumberOfProduct();
                        newProduct.shopID = shop.id;
                        newProduct.productID = productId;
                        newProduct.amount = soluong;
                        _context.NumberOfProduct.Add(newProduct);
                        await _context.SaveChangesAsync();
                    }

                }

            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }


        // GET: api/Products/get
        [HttpGet]
        [Route("get")]

        public async Task<ActionResult<List<JObject>>> GetAllProducts(
            [FromQuery(Name = "brandID")] int brands_id,
            [FromQuery(Name = "typeID")]  int types_id,
            [FromQuery(Name = "materialID")] int materials_Id,
            [FromQuery(Name = "name")] string name,int page)
            

        {

            List<Product> listProduct = new List<Product>();

            listProduct = await _context.Product.Where(a => a.delete != true).ToListAsync();
            if (brands_id != null && brands_id != 0)
            {
                listProduct = new List<Product>(listProduct.Where(a => a.brandID == brands_id));
                // listProduct = listProduct.Where(a => a.brandID == brands_id);

            }

            if (types_id != null && types_id != 0)
            {

                listProduct = new List<Product>(listProduct.Where(a => a.typeID == types_id));
                // listProduct = await listProduct.Where(a => a.typeID == types_id);

            }
            if (materials_Id != null && materials_Id != 0)
            {

                listProduct = new List<Product>(listProduct.Where(a => a.materialID == materials_Id));
            }
            if (name != null)
            {
                string keyword = convertToUnSign3(name);
              
               
                var newList = from product in _context.Product
                               where product.delete != true && product.alias.Contains(keyword)
                               select product;
                listProduct = new List<Product>(newList);


            }
            List<Product> newData = new List<Product>((from types in listProduct select types));

            if (page > 0) {
                int startIndex = (page - 1) * 15;
                newData = new List<Product>((from types in listProduct select types).Skip(startIndex).Take(15));
            }

            List<JObject> listJObject = new List<JObject>();
            foreach (var b in newData)
            {
                listJObject.Add(b.tranform());
            }

           

            Object pagination = GetPagiantion(page, listProduct.Count());

            List<JObject> newListObject = new List<JObject>();
            foreach (var b in listProduct)
            {

                newListObject.Add(b.tranfromObject(listJObject, pagination));

                break;

            }

            return newListObject;


        }

        //get detail sản phẩm đồng thời get tất cả sản phẩm trong cùng bộ sưu tập

        // GET: api/Products/detail/id=?1008
        [HttpGet]
        [Route("detail")]
        public async Task<ActionResult<List<ViewProductDetail>>> GetProduct(int id)
        {
            var product = await _context.Product.FindAsync(id);
            var brands = await _context.Brands.FindAsync(product.brandID);
            string brandName = "";
            if (brands != null)
            {
                brandName = brands.name;
            }
            string typeName = "";
            var types = await _context.Types.FindAsync(product.typeID);
            if (types != null)
            {
                typeName = types.name;
            }
            string materialName = "";

            var materials = await _context.Materials.FindAsync(product.materialID);
            if (materials != null)
            {
                materialName = materials.name;
            }
            List<Product> collectionProduct = new List<Product>();
            if (product.collectionID!=null && product.collectionID!=0)
            {
               
                var collection = _context.Product.Where(a => a.delete != true && a.collectionID == product.collectionID);
                if(collection!=null)
                {
                    collectionProduct = new List<Product>(collection);
                }

            }
            else
            {
                var listProductSameType = _context.Product.Where(a => a.delete != true && a.typeID == product.typeID).Take(10);
                collectionProduct = new List<Product>(listProductSameType);
            }
            List<ViewProductDetail> newListObject = new List<ViewProductDetail>();
            newListObject.Add(product.tranfromCollection(collectionProduct, product,brandName,typeName,materialName));


            if (product == null)
            {
                return NotFound();
            }

            return newListObject;
        }



        //get all product by shopID
        // GET: api/Products/by-shop
        [HttpGet]
        [Route("by-shop")]
        [Authorize(Roles = "Manager")]
        public async Task<ActionResult<List<JObject>>> GetAllProductByShop(int shopID,int page, string name)
        {
            int startIndex = (page - 1) * 15;
            int total = 0;
            List<Product> listProduct = new List<Product>();
            List<NumberOfProduct> listAvaiable = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.shopID == shopID));


            foreach (var item in listAvaiable)
            {
                Product product = _context.Product.Where(a => a.id == item.productID && a.delete != true).FirstOrDefault();
                if (product!=null)
                {
                    listProduct.Add(product);
                }
            }

            if (name != null)
            {
                string keyword = convertToUnSign3(name);
                var newList = listProduct.Where(a =>a!=null && a.alias.Contains(keyword)).ToList();
                listProduct = new List<Product>(newList);
            }
            total = listProduct.Count();
            listProduct = new List<Product>((listProduct).Skip(startIndex).Take(15)); 


            List<JObject> listJObject = new List<JObject>();
            foreach (var b in listProduct)
            {
                var numberOfProduct = await _context.NumberOfProduct.Where(a => a.shopID == shopID && a.productID == b.id).FirstOrDefaultAsync();
                Types type = _context.Types.Where(a => a.delete != true && a.id == b.typeID).FirstOrDefault();
                Materials material = _context.Materials.Where(ma => ma.delete != true && ma.id == b.materialID).FirstOrDefault();
                Brands brand = _context.Brands.Where(br => br.delete != true && br.id == b.brandID).FirstOrDefault();

            
                listJObject.Add(b.tranformProduct(numberOfProduct.amount,type,material,brand));
            }



            Object pagination = GetPagiantion(page, total);

            List<JObject> newListObject = new List<JObject>();
            foreach (var b in listProduct)
            {

                newListObject.Add(b.tranfromObject(listJObject, pagination));

                break;

            }

            return newListObject;

        }



        // PUT: api/Products/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<IActionResult> PutProduct(int id, [FromForm] FileUploadAPI obj)
        {
            bool check = false;
            //if (id != product.id)
            //{
            //    return BadRequest();
            //}

            try
            {
                var product = await _context.Product.FindAsync(id);
                string webRootPath = _IWebHostEnvironment.WebRootPath;

                product.name = obj.name;
                product.alias = convertToUnSign3(obj.name);
                product.percentOff = obj.percentOff != null ? Int32.Parse(obj.percentOff) : 0;
                product.describe = obj.describe;
                product.collectionID = obj.collectionID != null ? Int32.Parse(obj.collectionID) : 0;


                product.price = Int32.Parse(obj.price);

                var materialId = Int32.Parse(obj.materialId);
                var materials = await _context.Materials.FindAsync(materialId);
                if (materials != null)
                {
                    product.materialID = materialId;
                   
                }
                else
                {
                    check = true;
                }

                var brandId = Int32.Parse(obj.brandId);
                var brands = await _context.Brands.FindAsync(brandId);
                if (brands != null)
                {
                    product.brandID = brandId;
                   
                }
                else
                {
                    check = true;
                }

                var typeId = Int32.Parse(obj.typeId);
                var types = await _context.Types.FindAsync(typeId);
                if (types != null)
                {
                    product.typeID = typeId;
                   
                }
                else
                {
                    check = true;
                }

             

                if (obj.files != null)
                {

                    if (!Directory.Exists(_IWebHostEnvironment.WebRootPath + "/Upload/"))
                    {
                        Directory.CreateDirectory(_IWebHostEnvironment.WebRootPath + "/Upload/");
                    }
                    var extension_old = product.image;
                    if (System.IO.File.Exists((webRootPath + extension_old)))
                    {
                        System.IO.File.Delete((webRootPath + extension_old));
                    }

                    using (FileStream fileStream = System.IO.File.Create(webRootPath + "/Upload/" + obj.files.FileName))
                    {
                        obj.files.CopyTo(fileStream);
                        fileStream.Flush();


                        if (obj.files.FileName != null)
                        {
                            product.image = "/Upload/" + obj.files.FileName;

                        }




                    }
                }
               
                

                if (check != true)
                {
                    _context.Update(product);

                    await _context.SaveChangesAsync();
                }
               



                // _context.Entry(obj).State = EntityState.Modified;

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
    
        [HttpPost]
      //  [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<Product>> PostProduct([FromForm] FileUploadAPI obj)
        {
            bool check = false;
            Product newProduct = new Product();
            try
            {
                newProduct.name = obj.name;
                newProduct.alias = convertToUnSign3(obj.name);

                newProduct.percentOff = obj.percentOff != null ? Int32.Parse(obj.percentOff) : 0;
                newProduct.describe = obj.describe != null ? obj.describe : null;
                newProduct.collectionID=obj.collectionID!= null?Int32.Parse(obj.collectionID) : 0;
                newProduct.price = obj.price != null ? Int32.Parse(obj.price) : 0;
               
             


                if (obj.files.Length > 0)
                {
                    if (!Directory.Exists(_IWebHostEnvironment.WebRootPath + "/Upload/"))
                    {
                        Directory.CreateDirectory(_IWebHostEnvironment.WebRootPath + "/Upload/");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_IWebHostEnvironment.WebRootPath + "/Upload/" + obj.files.FileName))
                    {
                        obj.files.CopyTo(fileStream);
                        fileStream.Flush();
                  
                        newProduct.image = "/Upload/" + obj.files.FileName;
                        // newProduct.image = Hepler.getImage(obj.files);

                   
                    }
                }
                int brandId = Int32.Parse(obj.brandId);
                var brands = await _context.Brands.FindAsync(brandId);
                if (brands != null)
                {
                    newProduct.brandID = brandId;

                    newProduct.brands = brands;
                }
                else
                {
                    check = true;
                }
                int typeId = Int32.Parse(obj.typeId);
                var types = await _context.Types.FindAsync(typeId);
                if (types != null)
                {
                    newProduct.typeID = typeId;
                    newProduct.types = types;
                }
                else
                {
                    check = true;
                }

                var materialId = Int32.Parse(obj.materialId);
                var materials = await _context.Materials.FindAsync(materialId);
                if (materials != null)
                {
                    newProduct.materialID = materialId;

                    newProduct.materials = materials;
                }
                else
                {
                    check = true;
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (check != true)
            {
                _context.Product.Add(newProduct);
                await _context.SaveChangesAsync();
                //them sản phẩm vào bảng NumberOfProduct nếu sản phẩm này chưa có sẵn 
              //  AddItemNumberOfProduct(newProduct.id, obj.amount);
                List<NumberOfProduct> numberofproduct = new List<NumberOfProduct>(_context.NumberOfProduct.Where(a => a.productID == newProduct.id));
                if (numberofproduct.Count < 1)
                {
                    List<Shops> listShop = new List<Shops>(_context.Shops.Where(item => item.delete != true));
                    foreach (var shop in listShop)
                    {
                        NumberOfProduct numberOfProduct = new NumberOfProduct();
                        numberOfProduct.shopID = shop.id;
                        numberOfProduct.productID = newProduct.id;
                        numberOfProduct.amount = obj.amount;
                        _context.NumberOfProduct.Add(numberOfProduct);
                        await _context.SaveChangesAsync();
                    }

                }



            }
            return CreatedAtAction("GetProduct", new { id = newProduct.id }, newProduct);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "SuperAdmin,Manager")]
        public async Task<ActionResult<Product>> DeleteProduct(int id)
        {
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            var extension_old = product.image;
            string webRootPath = _IWebHostEnvironment.WebRootPath;
            if (System.IO.File.Exists((webRootPath + extension_old)))
            {
                System.IO.File.Delete((webRootPath + extension_old));
            }
            product.delete = true;
            product.image ="";
            _context.Update(product);
            // _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.id == id);
        }
    }
}
