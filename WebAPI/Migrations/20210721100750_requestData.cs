﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class requestData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "AspNetRoles",
            //    columns: table => new
            //    {
            //        Id = table.Column<string>(nullable: false),
            //        Name = table.Column<string>(maxLength: 256, nullable: true),
            //        NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
            //        ConcurrencyStamp = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetRoles", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "AspNetUsers",
            //    columns: table => new
            //    {
            //        Id = table.Column<string>(nullable: false),
            //        UserName = table.Column<string>(maxLength: 256, nullable: true),
            //        NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
            //        Email = table.Column<string>(maxLength: 256, nullable: true),
            //        NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
            //        EmailConfirmed = table.Column<bool>(nullable: false),
            //        PasswordHash = table.Column<string>(nullable: true),
            //        SecurityStamp = table.Column<string>(nullable: true),
            //        ConcurrencyStamp = table.Column<string>(nullable: true),
            //        PhoneNumber = table.Column<string>(nullable: true),
            //        PhoneNumberConfirmed = table.Column<bool>(nullable: false),
            //        TwoFactorEnabled = table.Column<bool>(nullable: false),
            //        LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
            //        LockoutEnabled = table.Column<bool>(nullable: false),
            //        AccessFailedCount = table.Column<int>(nullable: false),
            //        Discriminator = table.Column<string>(nullable: false),
            //        FullName = table.Column<string>(type: "nvarchar(50)", nullable: true),
            //        Delete = table.Column<bool>(nullable: true),
            //        Role = table.Column<string>(type: "nvarchar(50)", nullable: true),
            //        shopID = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetUsers", x => x.Id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Brands",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: false),
            //        country = table.Column<string>(nullable: false),
            //        alias = table.Column<string>(nullable: true),
            //        image = table.Column<string>(nullable: true),
            //        discription = table.Column<string>(nullable: true),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Brands", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Collections",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: false),
            //        alias = table.Column<string>(nullable: true),
            //        discription = table.Column<string>(nullable: true),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Collections", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Customer",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: true),
            //        email = table.Column<string>(nullable: true),
            //        provider = table.Column<string>(nullable: true),
            //        fullname = table.Column<string>(nullable: true),
            //        address = table.Column<string>(nullable: true),
            //        phone = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Customer", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Materials",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: false),
            //        alias = table.Column<string>(nullable: true),
            //        discription = table.Column<string>(nullable: true),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Materials", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "NumberOfProduct",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        productID = table.Column<int>(nullable: false),
            //        shopID = table.Column<int>(nullable: false),
            //        amount = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_NumberOfProduct", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Promotions",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        promotionCode = table.Column<string>(nullable: true),
            //        image = table.Column<string>(nullable: true),
            //        discription = table.Column<string>(nullable: true),
            //        alias = table.Column<string>(nullable: true),
            //        condition = table.Column<int>(nullable: false),
            //        moneyOff = table.Column<int>(nullable: false),
            //        startday = table.Column<string>(nullable: true),
            //        endDay = table.Column<string>(nullable: true),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Promotions", x => x.id);
            //    });

            migrationBuilder.CreateTable(
                name: "Request",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    shopId = table.Column<int>(nullable: false),
                    productId = table.Column<int>(nullable: false),
                    amount = table.Column<int>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    state = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Request", x => x.id);
                });

            //migrationBuilder.CreateTable(
            //    name: "ShoppingCart",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        customerID = table.Column<int>(nullable: false),
            //        productID = table.Column<int>(nullable: false),
            //        soluong = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_ShoppingCart", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Shops",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: false),
            //        alias = table.Column<string>(nullable: true),
            //        address = table.Column<string>(nullable: false),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Shops", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Types",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: false),
            //        alias = table.Column<string>(nullable: true),
            //        discription = table.Column<string>(nullable: true),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Types", x => x.id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "AspNetRoleClaims",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        RoleId = table.Column<string>(nullable: false),
            //        ClaimType = table.Column<string>(nullable: true),
            //        ClaimValue = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
            //            column: x => x.RoleId,
            //            principalTable: "AspNetRoles",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "AspNetUserClaims",
            //    columns: table => new
            //    {
            //        Id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        UserId = table.Column<string>(nullable: false),
            //        ClaimType = table.Column<string>(nullable: true),
            //        ClaimValue = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
            //        table.ForeignKey(
            //            name: "FK_AspNetUserClaims_AspNetUsers_UserId",
            //            column: x => x.UserId,
            //            principalTable: "AspNetUsers",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "AspNetUserLogins",
            //    columns: table => new
            //    {
            //        LoginProvider = table.Column<string>(nullable: false),
            //        ProviderKey = table.Column<string>(nullable: false),
            //        ProviderDisplayName = table.Column<string>(nullable: true),
            //        UserId = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
            //        table.ForeignKey(
            //            name: "FK_AspNetUserLogins_AspNetUsers_UserId",
            //            column: x => x.UserId,
            //            principalTable: "AspNetUsers",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "AspNetUserRoles",
            //    columns: table => new
            //    {
            //        UserId = table.Column<string>(nullable: false),
            //        RoleId = table.Column<string>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
            //        table.ForeignKey(
            //            name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
            //            column: x => x.RoleId,
            //            principalTable: "AspNetRoles",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //        table.ForeignKey(
            //            name: "FK_AspNetUserRoles_AspNetUsers_UserId",
            //            column: x => x.UserId,
            //            principalTable: "AspNetUsers",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "AspNetUserTokens",
            //    columns: table => new
            //    {
            //        UserId = table.Column<string>(nullable: false),
            //        LoginProvider = table.Column<string>(nullable: false),
            //        Name = table.Column<string>(nullable: false),
            //        Value = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
            //        table.ForeignKey(
            //            name: "FK_AspNetUserTokens_AspNetUsers_UserId",
            //            column: x => x.UserId,
            //            principalTable: "AspNetUsers",
            //            principalColumn: "Id",
            //            onDelete: ReferentialAction.Cascade);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Order",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        salePersonId = table.Column<string>(nullable: true),
            //        nameSalePerson = table.Column<string>(nullable: true),
            //        appointment = table.Column<DateTime>(nullable: false),
            //        customerName = table.Column<string>(nullable: true),
            //        customerPhoneNumber = table.Column<string>(nullable: true),
            //        customerEmail = table.Column<string>(nullable: true),
            //        address = table.Column<string>(nullable: true),
            //        promotionID = table.Column<int>(nullable: false),
            //        totalmoney = table.Column<int>(nullable: false),
            //        realmoney = table.Column<int>(nullable: false),
            //        moneyIsReduced = table.Column<int>(nullable: false),
            //        shopID = table.Column<int>(nullable: false),
            //        shopsid = table.Column<int>(nullable: true),
            //        delete = table.Column<bool>(nullable: false),
            //        isConfirmed = table.Column<bool>(nullable: false),
            //        delivering = table.Column<bool>(nullable: false),
            //        success_deliver = table.Column<bool>(nullable: false),
            //        isPayment = table.Column<bool>(nullable: false),
            //        cancel = table.Column<bool>(nullable: false),
            //        isCompleted = table.Column<bool>(nullable: false),
            //        Promotionsid = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Order", x => x.id);
            //        table.ForeignKey(
            //            name: "FK_Order_Promotions_Promotionsid",
            //            column: x => x.Promotionsid,
            //            principalTable: "Promotions",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Order_Shops_shopsid",
            //            column: x => x.shopsid,
            //            principalTable: "Shops",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Product",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: false),
            //        alias = table.Column<string>(nullable: true),
            //        image = table.Column<string>(nullable: false),
            //        describe = table.Column<string>(nullable: true),
            //        delete = table.Column<bool>(nullable: false),
            //        brandID = table.Column<int>(nullable: false),
            //        brandsid = table.Column<int>(nullable: true),
            //        typeID = table.Column<int>(nullable: false),
            //        typesid = table.Column<int>(nullable: true),
            //        materialID = table.Column<int>(nullable: false),
            //        materialsid = table.Column<int>(nullable: true),
            //        price = table.Column<int>(nullable: false),
            //        percentOff = table.Column<int>(nullable: false),
            //        collectionID = table.Column<int>(nullable: false),
            //        Collectionsid = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Product", x => x.id);
            //        table.ForeignKey(
            //            name: "FK_Product_Collections_Collectionsid",
            //            column: x => x.Collectionsid,
            //            principalTable: "Collections",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Product_Brands_brandsid",
            //            column: x => x.brandsid,
            //            principalTable: "Brands",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Product_Materials_materialsid",
            //            column: x => x.materialsid,
            //            principalTable: "Materials",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_Product_Types_typesid",
            //            column: x => x.typesid,
            //            principalTable: "Types",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "OrderDetail",
            //    columns: table => new
            //    {
            //        id = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        name = table.Column<string>(nullable: true),
            //        orderId = table.Column<int>(nullable: true),
            //        productID = table.Column<int>(nullable: true),
            //        image = table.Column<string>(nullable: true),
            //        price = table.Column<decimal>(nullable: false),
            //        quantityOfProduct = table.Column<int>(nullable: false),
            //        delete = table.Column<bool>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_OrderDetail", x => x.id);
            //        table.ForeignKey(
            //            name: "FK_OrderDetail_Order_orderId",
            //            column: x => x.orderId,
            //            principalTable: "Order",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //        table.ForeignKey(
            //            name: "FK_OrderDetail_Product_productID",
            //            column: x => x.productID,
            //            principalTable: "Product",
            //            principalColumn: "id",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_AspNetRoleClaims_RoleId",
            //    table: "AspNetRoleClaims",
            //    column: "RoleId");

            //migrationBuilder.CreateIndex(
            //    name: "RoleNameIndex",
            //    table: "AspNetRoles",
            //    column: "NormalizedName",
            //    unique: true,
            //    filter: "[NormalizedName] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AspNetUserClaims_UserId",
            //    table: "AspNetUserClaims",
            //    column: "UserId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AspNetUserLogins_UserId",
            //    table: "AspNetUserLogins",
            //    column: "UserId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AspNetUserRoles_RoleId",
            //    table: "AspNetUserRoles",
            //    column: "RoleId");

            //migrationBuilder.CreateIndex(
            //    name: "EmailIndex",
            //    table: "AspNetUsers",
            //    column: "NormalizedEmail");

            //migrationBuilder.CreateIndex(
            //    name: "UserNameIndex",
            //    table: "AspNetUsers",
            //    column: "NormalizedUserName",
            //    unique: true,
            //    filter: "[NormalizedUserName] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Order_Promotionsid",
            //    table: "Order",
            //    column: "Promotionsid");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Order_shopsid",
            //    table: "Order",
            //    column: "shopsid");

            //migrationBuilder.CreateIndex(
            //    name: "IX_OrderDetail_orderId",
            //    table: "OrderDetail",
            //    column: "orderId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_OrderDetail_productID",
            //    table: "OrderDetail",
            //    column: "productID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Product_Collectionsid",
            //    table: "Product",
            //    column: "Collectionsid");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Product_brandsid",
            //    table: "Product",
            //    column: "brandsid");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Product_materialsid",
            //    table: "Product",
            //    column: "materialsid");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Product_typesid",
            //    table: "Product",
            //    column: "typesid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "NumberOfProduct");

            migrationBuilder.DropTable(
                name: "OrderDetail");

            migrationBuilder.DropTable(
                name: "Request");

            migrationBuilder.DropTable(
                name: "ShoppingCart");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Promotions");

            migrationBuilder.DropTable(
                name: "Shops");

            migrationBuilder.DropTable(
                name: "Collections");

            migrationBuilder.DropTable(
                name: "Brands");

            migrationBuilder.DropTable(
                name: "Materials");

            migrationBuilder.DropTable(
                name: "Types");
        }
    }
}
