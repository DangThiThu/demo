﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class updateRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "state",
                table: "Request");

            migrationBuilder.AddColumn<bool>(
                name: "isconfirm",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "receive",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "request",
                table: "Request",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "sending",
                table: "Request",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isconfirm",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "receive",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "request",
                table: "Request");

            migrationBuilder.DropColumn(
                name: "sending",
                table: "Request");

            migrationBuilder.AddColumn<string>(
                name: "state",
                table: "Request",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
