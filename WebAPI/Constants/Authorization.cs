﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Constants
{
    public class Authorization
    {
        public enum Roles
        {
            SuperAdmin,
            Staff,
            Manager
        }
        public const String default_username = "SuperAdmin";
        public const String default_password = "Admin@123";
        public const String default_fullname = "DangThiThu";
        public const String default_email = "spktdangthithu123@gmail.com";
        public const String default_role = "SuperAdmin";

    }
}
